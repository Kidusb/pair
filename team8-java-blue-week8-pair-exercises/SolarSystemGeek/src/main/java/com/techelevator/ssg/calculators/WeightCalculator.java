package com.techelevator.ssg.calculators;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;

public class WeightCalculator {

	private String planetChoice;
	private double earthWeight;
	private Map<String, Double> weightMap = new HashMap<>();
	
	public WeightCalculator(String planetChoice, double earthWeight) {
		weightMap.put("Mercury", 0.37);
		weightMap.put("Venus", 0.9);
		weightMap.put("Mars", 0.38);
		weightMap.put("Jupiter", 2.65);
		weightMap.put("Saturn", 1.13);
		weightMap.put("Uranus", 1.09);
		weightMap.put("Neptune", 1.43);
}

	public double getAlienWeight() {
		 if(!weightMap.containsKey(planetChoice.toLowerCase())) {
	            return 0.0;
	        }
	        double conversion = weightMap.get(planetChoice.toLowerCase());
	        double newPlanetWeight = earthWeight * conversion;
	        
	        return Math.floor(newPlanetWeight);			
			
			
			
			
			
//		String planetName = request.getParameter("planet");
//		int earthWeight = Integer.parseInt(request.getParameter("earthWeight"));
//		double planetWeight = weightMap.get(planetName)*earthWeight;
//		request.setAttribute("planetWeight", planetWeight);
//		
//		return "alienWeightResult";
	
	}
	
}

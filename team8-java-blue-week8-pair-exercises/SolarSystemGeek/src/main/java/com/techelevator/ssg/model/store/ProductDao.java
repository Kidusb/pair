package com.techelevator.ssg.model.store;

import java.util.List;

import org.springframework.stereotype.Component;


@Component
public interface ProductDao {

	public List<Product> getAllProducts();

	public Product getProductById(Long productId);
}

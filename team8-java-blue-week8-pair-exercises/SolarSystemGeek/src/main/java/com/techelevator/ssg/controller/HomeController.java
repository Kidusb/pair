package com.techelevator.ssg.controller;


import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.ssg.calculators.AgeCalculator;
import com.techelevator.ssg.calculators.TravelCalculator;
import com.techelevator.ssg.calculators.WeightCalculator;
import com.techelevator.ssg.model.forum.ForumDao;
import com.techelevator.ssg.model.forum.ForumPost;
import com.techelevator.ssg.model.store.Product;
import com.techelevator.ssg.model.store.ProductDao;
import com.techelevator.ssg.model.store.ShoppingCart;




@Controller
public class HomeController {

	@Autowired 
	private ForumDao forumDao;
	
	@Autowired 
	private ProductDao productDao;
	
//	private Map<String, Double> weightMap = new HashMap<String, Double>();
//	private Map<String, Double> ageMap = new HashMap<String, Double>();
//	private Map<String, Double> travelMap = new HashMap<String, Double>();
//	private Map<String, Integer> methodMap = new HashMap<String, Integer>();
	
	@RequestMapping("/")
	public String displayHomePage() {
		return "homePage";
	}

	@RequestMapping("/alienAge")
	public String displayAlienAge() {
		return "alienAge";
	}
	
	@RequestMapping("/alienAgeResult")
	public String displayAlienAgeResult(@RequestParam String planetChoice, @RequestParam double earthAge, ModelMap map) {
		AgeCalculator calc = new AgeCalculator(planetChoice, earthAge);
		double x = calc.getAlienAge();
		map.put("calculator", x);
		return "alienAgeResult";
	}
	
	@RequestMapping("/alienWeight")
	public String displayAlienWeight() {
		return "alienWeight";
	}
	
	@RequestMapping("/alienWeightResult")
	public String displayAlienWeightResult(@RequestParam String planetChoice, @RequestParam double earthWeight, ModelMap map) {
		WeightCalculator calc = new WeightCalculator(planetChoice, earthWeight);
		map.put("calculator", calc);
		
		return "alienWeightResult";
	}
	
	@RequestMapping("/driveTime")
	public String displayDriveTime() {
		return "driveTime";
	}
	
	@RequestMapping("/driveTimeResult")
	public String displaydriveTimeResult(@RequestParam String planetChoice, @RequestParam String travelType, @RequestParam double earthTravel, ModelMap map) {
		TravelCalculator calc = new TravelCalculator(planetChoice, travelType, earthTravel);
		map.put("calculator", calc);
		return "driveTimeResult";
	}
	
	@RequestMapping(path="/newPost", method=RequestMethod.GET)
	public String showSpaceForum() {
		return "newPost";
	}
	
	@RequestMapping(path="/newPost", method=RequestMethod.POST)
	public String forumSubmission(@RequestParam String username, @RequestParam String subject, @RequestParam String message) {
		ForumPost post = new ForumPost();
		post.setUsername(username);
		post.setSubject(subject);
		post.setMessage(message);
		post.setDatePosted(LocalDateTime.now());
		forumDao.save(post);
	
	return "redirect:/spaceForum";
	}

	@RequestMapping(path="/spaceForum", method = RequestMethod.GET)
	public String displayAllPosts(HttpSession session, ModelMap map) {
		List<ForumPost> post= forumDao.getAllPosts();
		map.put("allPosts", post);
		return "allPosts";
	}

	@RequestMapping(path="/spaceStore", method = RequestMethod.GET)
	public String displayAllProducts(HttpSession session, ModelMap map) {
		List<Product> products= productDao.getAllProducts();
		map.put("allProducts", products);
		return "spaceStore";
	}
	
	@RequestMapping(path="/spaceStore/detail", method=RequestMethod.GET)
	public String showProductDetail(@RequestParam long id, ModelMap map) {
		Product product = productDao.getProductById(id);
		map.put("product", product);
		
		return "productDetails";
	}
	
	@RequestMapping(path="/shoppingCart", method=RequestMethod.POST)
	public String selectQuantity(@RequestParam Integer quantity, @RequestParam long id, HttpSession session) {
		if(session.getAttribute("cart") == null) {
			ShoppingCart shoppingCart = new ShoppingCart();
			shoppingCart.addItemToCart(productDao.getProductById(id), quantity);
			session.setAttribute("cart", shoppingCart);
		} else {
			ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute("cart");
			shoppingCart.addItemToCart(productDao.getProductById(id), quantity);
			session.setAttribute("cart", shoppingCart);
		}
		return "redirect:/shoppingCart";
	}
	
	@RequestMapping(path="/shoppingCart", method = RequestMethod.GET)
	public String displayShoppingCartProducts(HttpSession session) {
		return "shoppingCart";
	} 
	
}


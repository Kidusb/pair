package com.techelevator.ssg.calculators;

import java.util.HashMap;
import java.util.Map;

public class AgeCalculator {

	private String planetChoice;
	private double earthAge;
	private Map<String, Double> ageMap = new HashMap<>();
	
	public AgeCalculator(String planetChoice, double earthAge) {
		this.planetChoice = planetChoice;
		this.earthAge = earthAge;
		ageMap.put("mercury", 4.15);
		ageMap.put("venus", 1.62);
		ageMap.put("mars", 1.0);
		ageMap.put("jupiter", 0.084);
		ageMap.put("saturn", 0.034);
		ageMap.put("uranus", 0.012);
		ageMap.put("neptune", 0.006);
	}
    
	public double getAlienAge() {
        if(!ageMap.containsKey(planetChoice.toLowerCase())) {
            return 0.0;
        }
        double conversion = ageMap.get(planetChoice.toLowerCase());
        double newPlanetAge = earthAge * conversion;
        
        return newPlanetAge;
        
	}
	

}


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />


<!-- <session id="main-content">
<h1 class = "black">Alien Travel Calculator</h1> -->

<section id="main-content" class="main">
<h2>Alien Travel Calculator</h2>


<c:url value="/driveTimeResult" var="formAction" />
	<form method="GET" action="${formDriveTime}">
	
		<label for="planet">Choose a Planet </label> 
			<select id="planet" name="planet">
			<option value="mars">Mars</option>
			<option value="mercury">Mercury</option>
			<option value="saturn">Saturn</option>
			<option value="jupiter">Jupiter</option>
			<option value="neptune">Neptune</option>
			<option value="venus">Venus</option>
			<option value="uranus">Uranus</option>
		</select> 
		
		<br>
		<br>
		
		<label for="modeOfTravel">Choose a Way to Travel </label> 
			<select id="transportation" name="modeOfTravel">
			<option value="walking">Walking</option>
			<option value="car">Car</option>
			<option value="train">Train</option>
			<option value="boeing">Boeing</option>
			<option value="concorde">Concorde</option>
		</select> 
		
		<br>
		<br>
		
		<label for="age">Enter Your Earth Age </label> 
		<input type="text" id="earthAge" name="age" /> 
		
			<br>
			<br>
			<input type="submit" value="Calculate Age"/>			
	</form>

</section>






















<c:import url="/WEB-INF/jsp/common/footer.jsp" />
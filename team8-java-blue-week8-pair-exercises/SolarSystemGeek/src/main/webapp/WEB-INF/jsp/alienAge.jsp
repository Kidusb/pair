<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />


<section id="main-content" class="main">


<h2>Alien Age Calculator</h2>
	
	
<c:url value="/alienAgeResult" var="formAction" />
	<form method="GET" action="${formAction}">
	
		<label for="planetChoice">Choose a Planet</label> 
			<select name="planetChoice">
			<option value="mars">Mars</option>
			<option value="mercury">Mercury</option>
			<option value="saturn">Saturn</option>
			<option value="jupiter">Jupiter</option>
			<option value="neptune">Neptune</option>
			<option value="venus">Venus</option>
			<option value="uranus">Uranus</option>
		</select> 
		
		<br>
		<br>
		<label for="earthAge">Enter Your Earth Age </label> 
		<input type="text" name="earthAge" /> 
		
			<br>
			<input type="submit" name="Calculate Age" value="Calculate Age"/>			
	</form>
	
	<br>
	<br>

</section>


<c:import url="/WEB-INF/jsp/common/footer.jsp" />
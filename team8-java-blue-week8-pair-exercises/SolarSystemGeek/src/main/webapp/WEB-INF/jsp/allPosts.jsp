<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="/WEB-INF/jsp/common/header.jsp" />

<section id="main-content" class="main">
	<h2 class="black">Solar System Geek</h2>
	
	 <c:url value="/" var="base" /> 

	<div class="black">
	<h2><a href="${base}/newPost">Post a Message</a></h2>
	</div>
		
	<table class="posts">
		<c:forEach items="${allPosts}" var="post">
			<tr class="post">	
				<td class="post">
				<c:out value="${post.subject}"/>
				<br>
				<c:out value="${post.username}"/>
				<c:out value="${post.datePosted}"/>
				
				<br>
				<div>
				<c:out value="${post.message}"/>
				</div>
			<br><br><br>
			</td>
			</tr>
		</c:forEach>
	</table>
			
<br> 

</section>
	
<c:import url="/WEB-INF/jsp/common/footer.jsp" />
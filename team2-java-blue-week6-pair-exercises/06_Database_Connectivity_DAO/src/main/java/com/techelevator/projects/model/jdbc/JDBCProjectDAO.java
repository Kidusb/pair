package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Project;
import com.techelevator.projects.model.ProjectDAO;

public class JDBCProjectDAO implements ProjectDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCProjectDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Project> getAllActiveProjects() {
		
		List<Project> projects = new ArrayList<>();
		
		String proj = "SELECT project_id, name FROM project ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(proj);
		
		while (results.next()) {
			projects.add(mapRowToProject(results));
		}
		return projects;
	}

	@Override
	public void removeEmployeeFromProject(Long projectId, Long employeeId) {
		String sql = "DELETE FROM project_employee WHERE employee_id = ? AND project_id = ? ";
		jdbcTemplate.update(sql, projectId, employeeId);

	}

	@Override
	public void addEmployeeToProject(Long projectId, Long employeeId) {
	String sqladdEmployeeToProject = "INSERT INTO project_employee (project_id, employee_id) VALUES (?,?)";
		
		jdbcTemplate.update(sqladdEmployeeToProject, projectId, employeeId);
	}

	
	private Project mapRowToProject(SqlRowSet result) {
		Project projects = new Project();
		projects.setName(result.getString("name"));
		projects.setId(result.getLong("project_id"));
		
		return projects;
	}
	
}

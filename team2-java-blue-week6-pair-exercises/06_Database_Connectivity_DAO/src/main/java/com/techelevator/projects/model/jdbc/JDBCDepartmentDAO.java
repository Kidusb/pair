package com.techelevator.projects.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.DepartmentDAO;
import com.techelevator.projects.model.Employee;

public class JDBCDepartmentDAO implements DepartmentDAO {
	
	private JdbcTemplate jdbcTemplate;

	public JDBCDepartmentDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Department> getAllDepartments() {
		
		List<Department> departments = new ArrayList<>();
			
			String sqlGetAllDepartments = "SELECT department_id, name FROM department ";
			SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllDepartments);
			
			while (results.next()) {
				departments.add(mapRowToDepartment(results));
		}
		return departments;
	}

	@Override
	public List<Department> searchDepartmentsByName(String nameSearch) {
		List<Department> departments =  new ArrayList<>();
		
		String sql = "SELECT name FROM department WHERE name = ? ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sql, nameSearch);
		
		while (results.next()) {
			departments.add(mapRowToDepartment(results));
		}
		return departments;
	}

	@Override
	public void saveDepartment(Department updatedDepartment) {
		String sqlInsertDepartment = "INSERT INTO department(department_id, name)"
				+ "VALUES ( ?, ? )";
		
		updatedDepartment.setId(getNextDepartmentId());
		jdbcTemplate.update(sqlInsertDepartment, updatedDepartment.getId(), updatedDepartment.getName());
	}

//	@Override			for deleting purposes 
//	public void delete(long id) {
//		String sql = "DELETE FROM department WHERE department_id = ?";
//		jdbcTemplate.update(sql, id);
//	}
	
	@Override
	public Department createDepartment(Department newDepartment) {
		String sqlInsertDepartment = "INSERT INTO department(department_id, name) " 
				+ "VALUES ( ?, ? )";
		
		newDepartment.setId(getNextDepartmentId());
		
		jdbcTemplate.update(sqlInsertDepartment, newDepartment.getId(), newDepartment.getName());
	
		return newDepartment;
	}

	@Override
	public Department getDepartmentById(Long id) {
		Department theDepart = null;
		String sqlFindDepartmentById = "SELECT department_id, name FROM department WHERE department_id = ? ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindDepartmentById, id);
		
		if (results.next()) {
			theDepart = mapRowToDepartment(results);
		}
		return theDepart;
	}
	
	private long getNextDepartmentId() {
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_department_id')");
		if (nextIdResult.next()) {
			return nextIdResult.getLong(1);
		} else {
			throw new RuntimeException("Something went wrong while getting an id for the new city");
		}
	}
	
	private Department mapRowToDepartment(SqlRowSet result) {
		Department departs = new Department();
		departs.setId(result.getLong("department_id"));
		departs.setName(result.getString("name"));		
	return departs;
	}


}
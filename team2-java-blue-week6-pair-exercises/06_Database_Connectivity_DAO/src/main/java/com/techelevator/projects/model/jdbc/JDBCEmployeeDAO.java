package com.techelevator.projects.model.jdbc;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.Employee;
import com.techelevator.projects.model.EmployeeDAO;
import com.techelevator.projects.model.Project;

public class JDBCEmployeeDAO implements EmployeeDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCEmployeeDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Employee> getAllEmployees() {
		List<Employee> employee = new ArrayList<>();

		String employ = "SELECT employee_id, department_id, first_name, last_name, birth_date, gender, hire_date FROM employee ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(employ);

		while (results.next()) {
			employee.add(mapRowToEmployee(results));
		}
		return employee;
	}
	
	@Override
	public List<Employee> searchEmployeesByName(String firstNameSearch, String lastNameSearch) {
		
		List<Employee> employee =  new ArrayList<>();
		
		String searchName = "SELECT employee.employee_id, employee.department_id, first_name, last_name, birth_date, gender, hire_date FROM employee WHERE first_name = ? AND last_name = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(searchName, firstNameSearch, lastNameSearch);
		
		while (results.next()) {
			employee.add(mapRowToEmployee(results));
		}
		return employee;
	}

	@Override
	public List<Employee> getEmployeesByDepartmentId(long id) {
		List<Employee> employees = new ArrayList<>();
		String sqlFindEmployeeById = "SELECT employee.employee_id, employee.department_id, first_name, last_name, birth_date, gender, hire_date FROM employee JOIN department ON employee.department_id = department.department_id WHERE department.department_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindEmployeeById, id);
		
		while (results.next()) {
			Employee theEmployees = mapRowToEmployee(results);
			employees.add(theEmployees);
		}
		return employees;
	}

	@Override
	public List<Employee> getEmployeesWithoutProjects() {
		List<Employee> employees = new ArrayList<>();
		String sqlFindEmployeeByProjectId = "SELECT employee_id, department_id, first_name, last_name, birth_date, gender, hire_date FROM employee JOIN project ON employee.employee_id = project.project_id WHERE project.project_id IS NULL ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindEmployeeByProjectId);
		
		while (results.next() ) {
			Employee theEmployees = mapRowToEmployee(results);
			employees.add(theEmployees);
		}
		return employees;
	}

	@Override
	public List<Employee> getEmployeesByProjectId(Long projectId) {
		List<Employee> employees = new ArrayList<>();
		String sqlFindEmployeeByProjectId = "SELECT employee_id, department_id, first_name, last_name, birth_date, gender, hire_date FROM employee JOIN project ON employee.employee_id = project.project_id WHERE project.project_id = ? ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindEmployeeByProjectId, projectId);
		
		while (results.next()) {
			Employee theEmployees = mapRowToEmployee(results);
			employees.add(theEmployees);
		}
		return employees;
	}

	@Override
	public void changeEmployeeDepartment(Long employeeId, Long departmentId) {
		String sqlchangeEmployeeDepartment = "UPDATE employee SET department_id = ? WHERE employee_id = ?";
//				"UPDATE employee_id, department_id, first_name, last_name, birth_date, gender, hire_date SET department WHERE employee.department_id = ? AND employee.employee_id = ? ";
		
		jdbcTemplate.update(sqlchangeEmployeeDepartment, employeeId, departmentId);
	}
	
	private Employee mapRowToEmployee(SqlRowSet result) {
		Employee employees = new Employee();
		employees.setId(result.getLong("employee_id"));
		employees.setDepartmentId(result.getLong("department_id") );
		employees.setFirstName(result.getString("first_name"));
		employees.setLastName(result.getString("last_name"));
		employees.setBirthDay(result.getDate("birth_date").toLocalDate() );
		employees.setGender(result.getString("gender").charAt(0) );
		employees.setHireDate(result.getDate("hire_date").toLocalDate() );		
		
		return employees;
	}
}
	

		
//		Employee employee = new Employee();
//		employee.setFirstName(firstName);
//		employee.setLastName(lastName);
//		employee.setBirthDay(birthDate);
//		employee.setGender(gender);
//		employee.setHireDate(hireDate);
//		
//		String insertEmployee = "INSERT INTO employee (first_name, last_name, birth_date, gender, hier_date,) VALUES (?,?,?,?,?) RETURNING employee_id";
//		employee.setId(jdbcTemplate.queryForObject(insertEmployee, Long.class, firstName, lastName, birthDate) );
//
//		return employee; 
//	}

	

package com.techelevator.projects.jdbc.test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.projects.model.EmployeeDAO;
import com.techelevator.projects.model.Project;
import com.techelevator.projects.model.ProjectDAO;
import com.techelevator.projects.model.jdbc.JDBCProjectDAO;

public class JDBCProjectDAOIntegrationTest {

	private static final String TEST_PROJECTS = "BOB";
	private ProjectDAO dao;
	private JdbcTemplate jdbcTemplate; 

	private static SingleConnectionDataSource dataSource;

	@BeforeClass 
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/projects");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		dataSource.setAutoCommit(false);

	}
	@AfterClass 
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}
	@After 
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	@Before  
	public void setup() {
		String sqlInsertProject = "INSERT INTO project (project_id, name, from_date, to_date) " +
				"VALUES (7, ?, '2007-12-31', '2018-6-15')";
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertProject, TEST_PROJECTS);
		dao = new JDBCProjectDAO(dataSource);
	}

	@Test 
	public void get_all_projects() {
		String sqlGetAllProjects = "SELECT project_id, name, from_date, to_date FROM project ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllProjects);
		
		
		int y = jdbcTemplate.queryForList(sqlGetAllProjects).size();
		Assert.assertEquals(y, dao.getAllActiveProjects().size());
	}

//	String sqlGetAllEmployee = "SELECT employee_id, department_id, first_name, last_name, birth_date, gender, hire_date FROM employee";
//	
//	int x = jdbcTemplate.queryForList(sqlGetAllEmployee).size();
//			
//	Assert.assertEquals(x,  dao.getAllEmployees().size());
//	
	@Test
	public void remove_employee_from_project() {
		String sqlInsertProject = "SELECT project_id, employee_id FROM project_employee ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlInsertProject);
		results.next();
		Long employeeId = results.getLong("employee_id");
		Long projectId = results.getLong("project_id");
		dao.removeEmployeeFromProject(projectId, employeeId);
		
		String sqlInsertProject2 = "SELECT project_id, employee_id FROM project_employee WHERE employee_id = 2 AND project_id = 4 ";
		SqlRowSet results2 = jdbcTemplate.queryForRowSet(sqlInsertProject2);
		
		Assert.assertTrue(results2.next() );

	}
	// INSERT instead and compare values 
	//created a list and compared the two
	
	@Test
	public void add_employee_to_project() {
		dao.addEmployeeToProject(4L, 3L);
		String sqlAddEmployeeToProjects = "SELECT project_id, employee_id FROM project_employee WHERE project_id = 4 AND employee_id = 3 ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlAddEmployeeToProjects);
		Assert.assertTrue(results.next() );
	}
	/*
	 * was not working because we were testing a value that 
	 * was already there, we needed to test a value that was
	 * not already on the project...since we are adding to a 
	 * new project.. 
	 */

}

	


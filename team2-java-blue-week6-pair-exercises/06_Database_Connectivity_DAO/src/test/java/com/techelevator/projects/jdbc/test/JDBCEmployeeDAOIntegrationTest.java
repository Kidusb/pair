package com.techelevator.projects.jdbc.test;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.*;

import com.techelevator.projects.model.Employee;
import com.techelevator.projects.model.EmployeeDAO;
import com.techelevator.projects.model.jdbc.JDBCEmployeeDAO;

public class JDBCEmployeeDAOIntegrationTest {


	private EmployeeDAO dao;
	private JdbcTemplate jdbcTemplate;

	private static SingleConnectionDataSource dataSource;

	@BeforeClass 
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/projects");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		dataSource.setAutoCommit(false);

	}
	@AfterClass 
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}
	@After 
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}

	@Before 
	public void setup() {
		String sqlInsertEmployee = "INSERT INTO employee (employee_id, department_id, first_name, last_name, birth_date, gender, hire_date)" +
				"VALUES (13, 3, 'Quinn', 'Collins', '1992-11-28', 'M', '2017-05-08')";
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertEmployee);
		dao = new JDBCEmployeeDAO(dataSource);
	}

	@Test
	public void select_all_employees() {
		String sqlGetAllEmployee = "SELECT employee_id, department_id, first_name, last_name, birth_date, gender, hire_date FROM employee";

		int x = jdbcTemplate.queryForList(sqlGetAllEmployee).size();

		Assert.assertEquals(x,  dao.getAllEmployees().size());

	}
	@Test 
	public void search_employee_by_name() {

		List<Employee> employees = new ArrayList<>();
		String resultsName = "t";
		String searchName = "f";
		String sqlSearchEmployeeByName = "SELECT department_id, first_name, last_name, birth_date, gender, hire_date FROM employee " +
				"WHERE first_name = 'Flo' AND last_name = 'Henderson'";
		SqlRowSet rows = jdbcTemplate.queryForRowSet(sqlSearchEmployeeByName);
		while (rows.next() ) {
			resultsName = rows.getString("first_name");
		}
		employees = dao.searchEmployeesByName("Flo", "Henderson");
		for (Employee employee : employees) {
			searchName = employee.getFirstName();
		}
		Assert.assertEquals(resultsName, searchName);
	}

	@Test 
	public void search_employee_by_department_id() {
		List<Employee> employees = new ArrayList<>();
		String resultsName = "t";
		String searchName = "f";
		String sqlSearchEmployeeByDepartment = "SELECT employee.employee_id, employee.department_id, employee.first_name, employee.last_name, birth_date, gender, hire_date " 
				+ "FROM employee WHERE employee.department_id = 1";
		SqlRowSet rows = jdbcTemplate.queryForRowSet(sqlSearchEmployeeByDepartment);
		while(rows.next()) {
			if(rows.getString("first_name").equals("Flo")) {
				resultsName = rows.getString("first_name");
			}
		}
		employees = dao.getEmployeesByDepartmentId(1);
		for(Employee employee : employees) {
			if(employee.getFirstName().equals("Flo")) {
				searchName = employee.getFirstName();
			}
		}
		Assert.assertEquals(searchName, resultsName);
	}

	@Test 
	public void get_employees_without_projects() {
		List<Employee> employees = new ArrayList<>();
		String resultsName = "t";
		String searchName = "Gabreila";
		String sqlGetEmployeesWithoutProjects = "SELECT employee.employee_id, employee.department_id, employee.first_name, employee.last_name, birth_date, gender, hire_date" 
				+ " FROM employee LEFT JOIN project_employee ON project_employee.employee_id = employee.employee_id"
				+ " WHERE project_employee.employee_id IS NULL OR project_employee.project_id IS NULL";
		SqlRowSet rows = jdbcTemplate.queryForRowSet(sqlGetEmployeesWithoutProjects);
		while(rows.next()) {
			if(rows.getString("first_name").equals( "Gabreila" )) {
				resultsName = rows.getString("first_name");
			}
		}
		employees = dao.getEmployeesWithoutProjects();
		for(Employee employee : employees) {
			if(employee.getFirstName().equals("Gabreila")) {
				searchName = employee.getFirstName();
			}
		}
		Assert.assertEquals(resultsName, searchName);
	}


	@Test
	public void can_get_employees_by_project_id() {
		List<Employee> employees = new ArrayList<>();
		String resultsName = "t";
		String searchName = "f";
		String sqlInsertProjectEmployeeID = "INSERT INTO project_employee (project_id, employee_id) VALUES (5, 3)";
		jdbcTemplate.update(sqlInsertProjectEmployeeID);
		String sqlGetEmployeesBYProjectId = "SELECT employee.employee_id, department_id, first_name, last_name, birth_date, gender, hire_date" 
				+ " FROM employee JOIN project_employee ON project_employee.employee_id = employee.employee_id"
				+ " WHERE project_employee.project_id = 1";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetEmployeesBYProjectId);
		while(results.next()) {
			if(results.getString("first_name").equals("Sid")) {
				resultsName = results.getString("first_name");
			}
		}
		employees = dao.getEmployeesByProjectId(5L);
		for(Employee employee : employees) {
			if(employee.getFirstName().equals("Sid")) {
				searchName = employee.getFirstName();
			}
		}

		Assert.assertEquals(resultsName, searchName);
	}

	@Test
	public void can_change_employee_department() {
		dao.changeEmployeeDepartment(2L, 1L);
		String sqlChangeEmployeeDepartment = "SELECT employee_id, department_id, first_name, last_name, birth_date, gender, hire_date " 
				+ "FROM employee WHERE employee_id = 2 ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlChangeEmployeeDepartment);
		results.next();

		Assert.assertEquals(1L, results.getLong("department_id"));
	}

}


		
		



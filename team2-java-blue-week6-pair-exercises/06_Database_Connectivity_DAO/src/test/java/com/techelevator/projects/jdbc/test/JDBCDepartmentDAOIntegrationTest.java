package com.techelevator.projects.jdbc.test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.projects.model.Department;
import com.techelevator.projects.model.DepartmentDAO;
import com.techelevator.projects.model.jdbc.JDBCDepartmentDAO;

public class JDBCDepartmentDAOIntegrationTest {

	private DepartmentDAO dao;
	private JdbcTemplate jdbcTemplate; 
	private static SingleConnectionDataSource dataSource;

	@BeforeClass 
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/projects");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		dataSource.setAutoCommit(false);
	
	}
	@AfterClass 
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}
	@After 
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	@Before 
	public void setup() {
		String sqlInsertDepartment = "INSERT INTO department (department_id, name) " 
				+ "VALUES (7, 'Department Of TOMATO')";
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sqlInsertDepartment);
		dao = new JDBCDepartmentDAO(dataSource);
	
	}
	
	@Test 
	public void get_all_departments() {
		List<Department> departments = new ArrayList<>();
		boolean findDepartment = false;
		departments = dao.getAllDepartments();
		for(Department department : departments) {
			if(department.getName().equals("Department of TOMATO")) {
				findDepartment = true;
			}
		}
		Assert.assertTrue(findDepartment);
	}
	
		

}
	

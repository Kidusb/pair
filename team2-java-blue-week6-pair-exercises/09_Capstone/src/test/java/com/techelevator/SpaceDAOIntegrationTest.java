package com.techelevator;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.venue.JDBC.JDBCSpaceDAO;
import com.techelevator.venue.JDBC.JDBCVenueDAO;
import com.techelevator.venue.model.Space;
import com.techelevator.venue.model.SpaceDAO;
import com.techelevator.venue.model.VenueDAO;

public class SpaceDAOIntegrationTest extends  DAOIntegrationTest {
	

		private SpaceDAO dao;
		private static SingleConnectionDataSource dataSource;
		private JdbcTemplate jdbcTemplate;
		
		@BeforeClass
		public static void setupDataSource() {
			dataSource = new SingleConnectionDataSource();
			dataSource.setUrl("jdbc:postgresql://localhost:5432/excelsior-venues");
			dataSource.setUsername("postgres");
			dataSource.setPassword("postgres1");
			/*
			 * The following line disables autocommit for connections returned by this
			 * DataSource. This allows us to rollback any changes after each test
			 */
			dataSource.setAutoCommit(false);
		}
		
		@AfterClass
		public static void closeDataSource() throws SQLException {
			dataSource.destroy();
		}
		
		
		
		@After
		public void rollback() throws SQLException {
			dataSource.getConnection().rollback();
		}
		
		@Before
		public void setup() {
			 
			
			jdbcTemplate = new JdbcTemplate(dataSource);
			dao = new JDBCSpaceDAO(dataSource);
			
		}
		
		@Test
		public void get_space_info() {
			List<Space> space = new ArrayList<>();
			String resultsName = "t";
			String searchName = "f";
			String sqlSpaceInfo = "SELECT name, is_accessible, daily_rate, id, venue_id, open_from, open_to, max_occupancy "
					+ "FROM space WHERE venue_id = '15'";
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sqlSpaceInfo);
			while(rows.next()) {
				resultsName = rows.getString("id");
			}
			space = dao.getSpaceInfo();
			for (Space spaces: space ) {
				searchName = spaces.getName();
				
			}
			Assert.assertEquals(resultsName, searchName);
		}			
			
//			String sqlGetSpaceInfo = "SELECT name, is_accessible, daily_rate, id, venue_id, open_from, open_to, max_occupancy FROM space WHERE venue_id = 2";
//			SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetSpaceInfo);
//			
//			int x = jdbcTemplate.queryForList(sqlGetSpaceInfo).size();
//			
//			Assert.assertEquals(x, dao.getSpaceInfo().size());
			
			
		}
		
		


package com.techelevator;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.venue.JDBC.JDBCReservationDAO;
import com.techelevator.venue.JDBC.JDBCVenueDAO;
import com.techelevator.venue.model.Venue;
import com.techelevator.venue.model.VenueDAO;

public class VenuDAOIntegrationTest extends  DAOIntegrationTest {

	private VenueDAO dao;
	private static SingleConnectionDataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/excelsior-venues");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		/*
		 * The following line disables autocommit for connections returned by this
		 * DataSource. This allows us to rollback any changes after each test
		 */
		dataSource.setAutoCommit(false);
	}
	
	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}
	
	
	
	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	
	@Before
	public void setup() {
//		String sqlInsertProject = "INSERT INTO venues ()"
		
		jdbcTemplate = new JdbcTemplate(dataSource);
		dao = new JDBCVenueDAO(dataSource);
		
	}
	
	@Test
	public void get_all_venues() {
		String sqlGetAllvenues = "SELECT id, name, city_id, description FROM venue";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAllvenues);
		
		int y = jdbcTemplate.queryForList(sqlGetAllvenues).size();
		Assert.assertEquals(y, dao.getAllVenues().size());
	}
	
	
}
	
	


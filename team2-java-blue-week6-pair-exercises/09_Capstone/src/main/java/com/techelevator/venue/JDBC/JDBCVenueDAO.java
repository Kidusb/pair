package com.techelevator.venue.JDBC;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.venue.model.Venue;
import com.techelevator.venue.model.VenueDAO;

public class JDBCVenueDAO implements VenueDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCVenueDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
		
	@Override
	public List<Venue> getAllVenues() {
		List<Venue> venues = new ArrayList<Venue>();
		String sqlGetAllVenues = "SELECT venue.id AS venue_id, venue.name AS venue_name, "
				+ "venue.city_id AS city_id, venue.description AS venue_description, city.name "
				+ "AS city_name, city.state_abbreviation AS state_abbreviation, STRING_AGG(category.name, ',' ) AS category_name FROM venue"
				+ " JOIN city ON venue.city_id = city.id"
				+ " LEFT JOIN category_venue ON venue.id = category_venue.venue_id"
				+ " LEFT JOIN category ON category_venue.category_id = category.id"
				+ " GROUP BY venue.id, city.name, city.state_abbreviation";
		
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetAllVenues);
		while (result.next()) {
			Venue venue = mapRowToVenue(result);
			venues.add(venue);
		}
		return venues;
	}
		
	@Override
	public Venue getVenueById(long id) {	
		String sqlGetAllVenues = "SELECT venue.id AS venue_id, venue.name AS venue_name, venue.city_id AS city_id, venue.description "
				+ "AS venue_description, city.name AS city_name, city.state_abbreviation AS state_abbreviation, "
				+ "STRING_AGG(category.name, ',' ) AS category_name FROM venue JOIN city ON venue.city_id = city.id"
				+ " LEFT JOIN category_venue ON venue.id = category_venue.venue_id"
				+ " LEFT JOIN category ON category_venue.category_id = category.id" + " WHERE venue.id = ?"
				+ " GROUP BY venue.id, city.name, city.state_abbreviation";;
		SqlRowSet result = jdbcTemplate.queryForRowSet(sqlGetAllVenues, id);
		Venue venue = null;
		while (result.next()) {
			venue = mapRowToVenue(result);
		}
		return venue;
	}
	
	private Venue mapRowToVenue(SqlRowSet result) {
		Venue venue = new Venue();
		venue.setId(result.getLong("venue_id"));
		venue.setName(result.getString("venue_name"));
		venue.setCity_id(result.getLong("city_id"));
		venue.setDescription(result.getString("venue_description"));
		venue.setCityName(result.getString("city_name"));
		venue.setStateAbbrevation(result.getString("state_abbreviation"));
		venue.setCategory(result.getString("category_name"));
		
		return venue;
	}	
}

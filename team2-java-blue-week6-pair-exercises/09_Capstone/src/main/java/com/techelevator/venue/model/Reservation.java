package com.techelevator.venue.model;

import java.time.LocalDate;

public class Reservation {

	
	private Long reservation_id;
	private Long number_of_attendess;
	private LocalDate start_date;
	private LocalDate end_date;
	private String reserved_for;
	private Long space_id;
	private String venue_name;
	private String space_name;
	private double daily_rate;
	
	
	public String getVenue_name() {
		return venue_name;
	}
	public void setVenue_name(String venue_name) {
		this.venue_name = venue_name;
	}
	public String getSpace_name() {
		return space_name;
	}
	public void setSpace_name(String space_name) {
		this.space_name = space_name;
	}
	public double getDaily_rate() {
		return daily_rate;
	}
	public void setDaily_rate(double daily_rate) {
		this.daily_rate = daily_rate;
	}
	public Long getReservation_id() {
		return reservation_id;
	}
	public void setReservation_id(Long reservation_id) {
		this.reservation_id = reservation_id;
	}
	public Long getSpace_id() {
		return space_id;
	}
	public void setSpace_id(Long space_id) {
		this.space_id = space_id;
	}
	public Long getNumber_of_attendess() {
		return number_of_attendess;
	}
	public void setNumber_of_attendess(Long number_of_attendess) {
		this.number_of_attendess = number_of_attendess;
	}
	public LocalDate getStart_date() {
		return start_date;
	}
	public void setStart_date(LocalDate start_date) {
		this.start_date = start_date;
	}
	public LocalDate getEnd_date() {
		return end_date;
	}
	public void setEnd_date(LocalDate end_date) {
		this.end_date = end_date;
	}
	public String getReserved_for() {
		return reserved_for;
	}
	public void setReserved_for(String reserved_for) {
		this.reserved_for = reserved_for;
	}
	
	
	
	
	
}

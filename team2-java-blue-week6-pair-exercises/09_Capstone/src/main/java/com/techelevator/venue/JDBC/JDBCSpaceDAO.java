package com.techelevator.venue.JDBC;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.venue.model.Space;
import com.techelevator.venue.model.SpaceDAO;
import com.techelevator.venue.model.Venue;

public class JDBCSpaceDAO implements SpaceDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCSpaceDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}


	@Override
	public List<Space> getAllSpacesById(long venueId) {
		List<Space> spaces = new ArrayList<>();
		String sqlGetSpaceInfo = "SELECT space.id AS space_id, space.venue_id AS venue_id, "
				+ "space.name AS space_name, space.is_accessible AS is_accessible, space.open_from AS open_from, "
				+ "space.open_to AS open_to, CAST (space.daily_rate AS numeric) AS daily_rate, space.max_occupancy "
				+ "AS max_occupancy, venue.name AS venue_name FROM space JOIN venue ON space.venue_id = venue.id WHERE venue.id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetSpaceInfo, venueId);
		while(results.next()) {
			Space space = mapRowToSpace(results);
			spaces.add(space);
		}
		return spaces;
	}

	@Override 
	public List <Space> getSpaceInfo(LocalDate userStartDate, int numberOfDays, int numberAttending, long venueId) {
		LocalDate userEndDate = userStartDate.plusDays(numberOfDays);
		List<Space> spaces = new ArrayList<>();
		String sqlGetAvaliableSpaces = "SELECT space.id AS space_id, space.venue_id AS venue_id, space.name AS space_name, "
				+ "space.is_accessible AS is_accessible, space.open_from AS open_from, space.open_to AS open_to, "
				+ "CAST (space.daily_rate AS numeric) AS daily_rate, space.max_occupancy AS max_occupancy, venue.name AS venue_name "
				+ "FROM space JOIN venue on space.venue_id = venue.id WHERE (space.max_occupancy > ?) AND (space.venue_id = ?) "
				+ "AND ((space.open_from IS NULL AND space.open_to IS NULL) OR (space.id NOT IN (SELECT space_id FROM reservation "
				+ "WHERE (? > start_date AND ? < end_date) OR (? > start_date AND ? < end_date) GROUP BY space_id)))";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetAvaliableSpaces, numberAttending, venueId, userStartDate, userStartDate, userEndDate, userEndDate);
		while (results.next()) {
			Space space = mapRowToSpace(results);
			spaces.add(space);
		}
		return spaces;
	}

	private Space mapRowToSpace(SqlRowSet result) {
		Space space = new Space();
		space.setDaily_rate(result.getDouble("daily_rate"));
		space.setId(result.getLong("space_id"));
		space.setIs_accessible(result.getBoolean("is_accessible"));
		space.setMax_occupancy(result.getLong("max_occupancy"));
		space.setName(result.getString("space_name"));
		space.setOpen_from(result.getLong("open_from"));
		space.setOpen_to(result.getLong("open_to"));
		space.setVenue_id(result.getLong("venue_id"));
		return space;
	}

}
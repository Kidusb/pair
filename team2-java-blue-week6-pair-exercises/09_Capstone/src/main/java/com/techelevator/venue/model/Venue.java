package com.techelevator.venue.model;

public class Venue {

	private Long id;
	private String name;
	private Long city_id;
	private String description;
	private String city_name;
	private String state_abbrevation;
	private String category;
	private String category_name;
	
	
	public String getCityName() {
		return city_name;
	}
	public void setCityName(String cityName) {
		this.city_name = cityName;
	}
	public String getStateAbbrevation() {
		return state_abbrevation;
	}
	public void setStateAbbrevation(String abbrevation) {
		state_abbrevation = abbrevation;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getCity_id() {
		return city_id;
	}
	public void setCity_id(Long city_id) {
		this.city_id = city_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}

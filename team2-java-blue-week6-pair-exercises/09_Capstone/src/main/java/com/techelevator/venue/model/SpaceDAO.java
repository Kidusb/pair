package com.techelevator.venue.model;

import java.time.LocalDate;
import java.util.List;

public interface SpaceDAO {
	
	List <Space> getAllSpacesById(long spaceId);

	List<Space> getSpaceInfo(LocalDate userStartDate, int numberOfDays, int numberAttending, long venueId);

}

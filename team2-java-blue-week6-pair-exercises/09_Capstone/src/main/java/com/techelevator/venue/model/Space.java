package com.techelevator.venue.model;

import java.time.LocalDate;

import org.springframework.cglib.core.Local;

public class Space {

	private Long id;
	private Long venue_id;
	private String name;
	private Double daily_rate;
	private boolean is_accessible;
	private long open_from;
	private long open_to;
	private Long max_occupancy;
	private String venue_name;
	
	
	
	public String getVenue_name() {
		return venue_name;
	}
	public void setVenue_name(String venue_name) {
		this.venue_name = venue_name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVenue_id() {
		return venue_id;
	}
	public void setVenue_id(Long venue_id) {
		this.venue_id = venue_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getDaily_rate() {
		return daily_rate;
	}
	public void setDaily_rate(Double daily_rate) {
		this.daily_rate = daily_rate;
	}
	public boolean is_accessible() {
		return is_accessible;
	}
	public void setIs_accessible(boolean is_accessible) {
		this.is_accessible = is_accessible;
	}
	public long getOpen_from() {
		return open_from;
	}
	public void setOpen_from(long open_from) {
		this.open_from = open_from;
	}
	public long getOpen_to() {
		return open_to;
	}
	public void setOpen_to(long open_to) {
		this.open_to = open_to;
	}
	public Long getMax_occupancy() {
		return max_occupancy;
	}
	public void setMax_occupancy(Long max_occupancy) {
		this.max_occupancy = max_occupancy;
	}
	
	
	
}

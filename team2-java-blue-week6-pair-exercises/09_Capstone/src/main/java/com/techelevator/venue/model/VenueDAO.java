package com.techelevator.venue.model;

import java.util.List;

public interface VenueDAO   {

	public List<Venue> getAllVenues();
	
	public Venue getVenueById(long id);
	
}

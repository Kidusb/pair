package com.techelevator.venue.model;

public interface ReservationDAO {

	Reservation insertReservation(Reservation reservation);
	
	Reservation getReservationsById(long id);
		
}

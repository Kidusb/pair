package com.techelevator.venue.JDBC;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.venue.model.Reservation;
import com.techelevator.venue.model.ReservationDAO;

public class JDBCReservationDAO implements ReservationDAO {

	private JdbcTemplate jdbcTemplate;

	public JDBCReservationDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}


	@Override
	public Reservation insertReservation(Reservation reservation) {		
		String sqlInsertReservaton = "INSERT INTO reservation (reservation_id, space_id, number_of_attendees, start_date, "
				+ "end_date, reserved_for) VALUES (DEFAULT, ?, ?, ?, ?, ?) RETURNING reservation_id";	
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlInsertReservaton, reservation.getSpace_id(), 
				reservation.getNumber_of_attendess(), reservation.getStart_date(), reservation.getEnd_date(), reservation.getReserved_for() );
		results.next();
		long reservationId = results.getLong("reservation_id");
		reservation.setReservation_id(reservationId);	
		return reservation;
	}

	@Override
	public Reservation getReservationsById(long id) {
		String sqlSelectReservation = "SELECT reservation.reservation_id AS reservation_id, reservation.space_id "
				+ "AS space_id, reservation.number_of_attendees AS number_of_attendees, reservation.start_date "
				+ "AS start_date, reservation.end_date AS end_date, reservation.reserved_for AS reserved_for, venue.name "
				+ "AS venue_name, space.name AS space_name, CAST (space.daily_rate AS numeric) AS daily_rate FROM reservation "
				+ "JOIN space ON reservation.space_id = space.id JOIN venue ON space.venue_id = venue.id WHERE reservation_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectReservation, id);
		Reservation reservation = null;
		if (results.next() ) {
			reservation = mapRowToReservation(results);
		}
		return reservation;
	}

	public Reservation getReserbationWithin30Days(long id) {
		String sqlSelectReservation = "SELECT reservation.reservation_id AS reservation_id30, reservation.space_id "
				+ "AS space_id30, reservation.number_of_attendees AS number_of_attendees30, reservation.start_date "
				+ "AS start_date30, reservation.end_date AS end_date30, reservation.reserved_for AS reserved_for30, "
				+ "venue.name AS venue_name30, space.name AS space_name30, CAST (space.daily_rate AS numeric) AS daily_rate30 "
				+ "FROM reservation JOIN space ON reservation.space_id = space.id JOIN venue ON space.venue_id = venue.id "
				+ "WHERE reservation_id30 = ? AND start_date30 >= 02/01/2020";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectReservation, id);
		Reservation reservation = null;
		if (results.next() ) {
			reservation = mapRowToReservation(results);
		}
		return reservation;
	}

	private Reservation mapRowToReservation(SqlRowSet result) {
		Reservation reserve = new Reservation();

		reserve.setNumber_of_attendess(result.getLong("number_of_attendees"));
		reserve.setReservation_id(result.getLong("reservation_id"));
		reserve.setReserved_for(result.getString("reserved_for"));
		reserve.setSpace_id(result.getLong("space_id"));
		if(result.getDate("start_date") != null ) {
			reserve.setStart_date(result.getDate("start_date").toLocalDate() );
		}
		if(result.getDate("end_date") != null ) {
			reserve.setEnd_date(result.getDate("end_date").toLocalDate() );
		}
		reserve.setVenue_name(result.getString("venue_name"));
		reserve.setSpace_name(result.getString("space_name"));
		reserve.setDaily_rate(result.getDouble("daily_rate"));
		return reserve;
	}

}
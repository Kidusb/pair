package com.techelevator.view;

import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Scanner;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

import com.techelevator.venue.JDBC.*;
import com.techelevator.venue.model.*;


public class ExcelsiorCLI {

	//Main Menu

	private static final String MAIN_MENU_OPTION_LIST_VENUES = "1";
	private static final String MAIN_MENU_OPTION_TO_QUIT = "Q";

	public String mainMenu() {
		System.out.println("   " + MAIN_MENU_OPTION_LIST_VENUES + ") List Venue");
		System.out.println("   " + MAIN_MENU_OPTION_TO_QUIT + ") Quit");
		System.out.println();
		System.out.print("Please make a selection >> " );
		return in.nextLine();
	}

	//Return to previous page

	private static final String RETURN_TO_PREVIOUS_SCREEN_OPTION = "R";

	//View Menu

	private static final String VENUE_DETAIL_MENU_OPTION_TO_VIEW_SPACES = "1";
	private static final String VENUE_DETAIL_MENU_OPTION_TO_SEARCH_FOR_RESERVATION = "2";

	//Space Menu

	private static final String SPACE_MENU_OPTION_TO_RESERVE_A_SPACE = "1";

	//Reservation Menu

	private static final String RESERVATION_MENU_OPTION_LIST_VENUE = "1";
	private static final String RESERVATION_MENU_DISPLAY_RESERVATION = "D";
	private static final String RESERVATION_MENU_QUIT = "Q";


	private Menu menu; 
	private VenueDAO venueDAO;
	private SpaceDAO spaceDAO;
	private ReservationDAO reservationDAO;

	private long userSelectedVenue;
	private int spaceToReserve;
	private int numberOfDaysReserved;
	private int numberAttending;
	private String reservationName;
	private LocalDate start_date;
	//extra testing
	//	private String spaceSelection;
	//	private String searchedReservation;


	private final Scanner in = new Scanner(System.in);

	public static void main(String[] args) throws Exception {
		Menu menu = new Menu();

		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/excelsior-venues");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");

		ExcelsiorCLI application = new ExcelsiorCLI(dataSource, menu);
		application.run();

	}

	public ExcelsiorCLI(DataSource dataSource, Menu menu) {
		venueDAO = new JDBCVenueDAO(dataSource);
		spaceDAO = new JDBCSpaceDAO(dataSource);
		reservationDAO = new JDBCReservationDAO(dataSource);
		this.menu = menu;

	}

	public void run() {
		//this method handles the main menu
		menu.displayApplicationBanner();
		while(true) {
			menu.printHeading("  What would you like to do?");
			String choice = mainMenu();
			if(choice.equals(MAIN_MENU_OPTION_LIST_VENUES)) {
				handleListVenue();
			} else if (choice.equals(MAIN_MENU_OPTION_TO_QUIT) || choice.equals("q")) {
				//this should be touppercase or lowercase, we cannot assume the user
				//will give us the correct information
				System.out.println();
				System.out.println("Thank you for your service!");
				break;
				//create private void method to handle the list of menus
			} else {
				menu.displayErrorMessage("\nInvalid input, please try again\n");
				//this will thank the user for coming.. 
			}
		}
	}

	private void handleListVenue() {
		//handles listing venues correctly
		while(true) {
			menu.printHeading("Which venue would you like to view?");
			List<Venue> venues = venueDAO.getAllVenues();
			String choice = venueMenu(venues);
			if (choice.equals(RETURN_TO_PREVIOUS_SCREEN_OPTION) || choice.equals("r") ) {
				//this should be touppercase or lowercase, we cannot assume the user
				//will give us the correct information
				break;
			}
			try {
				Long.parseLong(choice);
				//user choice into a single Long value
			}catch (NumberFormatException e) {
				menu.displayErrorMessage("Invalid input, please try again\n");
				continue;
			}
			userSelectedVenue = Long.parseLong(choice);
			if (userSelectedVenue > 0 && userSelectedVenue <= venues.size() ) {
				listVenueDetails(userSelectedVenue);
			} else {
				menu.displayErrorMessage("Invalid input, please try again\n");
			}
		}
	}

	private void listVenueDetails(long listedVenues) throws NullPointerException {
		// handles the list venue detail menu
		while(true) {
			Venue venue = venueDAO.getVenueById(userSelectedVenue);
			String choice = venueDetailMenu(venue);
			if (choice.equals(VENUE_DETAIL_MENU_OPTION_TO_VIEW_SPACES) ) {
				listSpaces();
			}else if(choice.equals(VENUE_DETAIL_MENU_OPTION_TO_SEARCH_FOR_RESERVATION) ) {
				handleSearchForReservation();
			}else if (choice.equals(RETURN_TO_PREVIOUS_SCREEN_OPTION) || choice.equals("r") ) {
				break;
			} else {
				menu.displayErrorMessage("\nInvalid input, please try again\n");
			}
		}
	}

	private void listSpaces() { 
		//handles the list spaces menu
		while (true) {
			List<Space> space = spaceDAO.getAllSpacesById(userSelectedVenue);
			String choice = spaceMenu(space);
			if(choice.equals(SPACE_MENU_OPTION_TO_RESERVE_A_SPACE) ) {
				reserveSpace();
			}else if (choice.equals(RETURN_TO_PREVIOUS_SCREEN_OPTION) || choice.equals("r") ) {
				break;
			} else {
				menu.displayErrorMessage("\nInvalid input, please try again\n");
			}
		}
	}

	private void handleSearchForReservation() {
		//handles search for reservation menu
		while (true) {
			List<Space> space = spaceDAO.getAllSpacesById(userSelectedVenue);
			String choice = searchForReservationMenu(space);
			if(choice.equals(RESERVATION_MENU_OPTION_LIST_VENUE) ) {
				handleListVenue();
			}else if (choice.equals(RESERVATION_MENU_DISPLAY_RESERVATION) || choice.equals("d") ) {
				handleDisplayReservations();
			}else if(choice.equals(RESERVATION_MENU_QUIT) || choice.equals("q") ) {
				break;
			} else {
				menu.displayErrorMessage("\nInvalid input, please try again\n");
			}
		}
	}


	private void reserveSpace() {
		//checks user input for spaces to reserve
		while (true) {
			String userStartDate = reservationStartDate();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			try {
				start_date = LocalDate.parse(userStartDate, formatter);
			}catch (Exception e) {
				menu.displayErrorMessage("\nInvalid format, please use format: (dd/mm/yyyy) ");
				continue;
			}
			String numberOfDays = numberOfDaysReserved();
			try {
				numberOfDaysReserved = Integer.parseInt(numberOfDays);
			} catch (NumberFormatException e) {
				menu.displayErrorMessage("\nInvalid input, please try again\n");
				continue;
			}
			if (numberOfDaysReserved <= 0) {
				menu.displayErrorMessage("Days reserved must be bigger than 0, please try again\n");
				continue;
			} else {
				String listOfAttendingParty = numberAttending();
				try {
					numberAttending = Integer.parseInt(listOfAttendingParty);
				} catch (NumberFormatException e) {
					menu.displayErrorMessage("\nInvalid input, please try again\n");
					continue;
				}
				if (numberAttending <= 0) {
					menu.displayErrorMessage("\nInvalid input, please try again\n");
					continue;
				}
				availableSpaces();
			}
		}
	}

	private void availableSpaces() {
		//populates the avaliable spaces
		/*outer:*/ while(true) {
			//tried to use outer: break to break out of current loop and back to original starting point....
			List<Space> spaces = spaceDAO.getSpaceInfo(start_date, numberOfDaysReserved, numberAttending, userSelectedVenue);
			String choice = reservedSpaceDisplay(spaces, numberOfDaysReserved);
			try {
				spaceToReserve = Integer.parseInt(choice);
			} catch (NumberFormatException e) {
				menu.displayErrorMessage("\nInvalid input, please try again\n");
				continue;
			}
			if (spaceToReserve == 0) {
//				run();
				System.out.println("Thank you for your time!");
				System.exit(0);
//				return;
				//if 0 is entered we will exit this menu 
			} else if (checkForAvaliableSpaces(spaces, spaceToReserve)) {
				handleReservations();
				// takes us to reserve space
			} else {
				menu.displayErrorMessage("\nInvalid input, please make your selection from the following spaces\n");
				continue;
			}
		}
	}

	private void handleReservations() {
		//handle user input for reservation 
		while(true) {
			reservationName = personMakingReservation();
			addToReservation();
		}
	}

	private String personMakingReservation() {
		System.out.println("Who is this reservation for?");
		return in.nextLine();
	}

	private void addToReservation() {
		//handles adding reservation 
		Reservation reservation = new Reservation();
		reservation.setSpace_id((long) spaceToReserve);
		reservation.setStart_date(start_date);
		reservation.setEnd_date(start_date.plusDays(numberOfDaysReserved));
		reservation.setNumber_of_attendess((long) numberAttending);
		reservation.setReserved_for(reservationName);
		reservation = reservationDAO.insertReservation(reservation);
		reservation = reservationDAO.getReservationsById(reservation.getReservation_id());
		handleConfirmationMenu(reservation);
		System.exit(0);
		//lets go back to the main menu, so the program doesnt shut down unless we quit
		//Diamond problem
	}

	private void handleConfirmationMenu(Reservation reservation) {
		long differenceInDays = ChronoUnit.DAYS.between(reservation.getStart_date(), reservation.getEnd_date());
		System.out.println();
		System.out.println("-----------------------------------------------"
				+ "------------------------------------------------------------------------------");
		System.out.println("Thanks for submitting your reservation! The details for your event are listed below:\n");
		System.out.println();
		System.out.println("Confirmation #: " + reservation.getReservation_id() );
		System.out.println("Venue: " + reservation.getVenue_name() );
		System.out.println("Space: " + reservation.getSpace_name() );
		System.out.println("Reserved For: " + reservation.getReserved_for() );
		System.out.println("Attendees: " + reservation.getNumber_of_attendess() );
		System.out.println("Arrival Date: " + reservation.getStart_date() );
		System.out.println("Depart Date: " + reservation.getEnd_date() );
		System.out.println("Total Cost: $" + (reservation.getDaily_rate() * differenceInDays) );
		System.out.println();
		System.out.println();
		System.out.println();
	}

	private void handleDisplayReservations() {
		System.out.println("This Feature is currently not avaliable, Please try again at a later time");
	}

	private boolean checkForAvaliableSpaces(List<Space> spaces, int spaceNumber) {
		for(Space space : spaces) {
			if (space.getId() == spaceNumber) {
				return true;
			}
		}
		return false;
	}

	private String reservedSpaceDisplay(List<Space> spaces, int numberOfDays) {
		System.out.println("The following spaces are available based on your needs:\n");
		System.out.println();
		System.out.println("------------------------------------------"
				+ "-----------------------------------------------------------------------------------");
		System.out.println();
		System.out.printf("%-10s %-25s %-20s %-10s %-15s %-10s%n", "Space #", "Name", "Daily Rate", "Max. Occup.", "Accessible?", "Total Cost");
		listOfAvaliableSpaces(spaces, numberOfDays);
		System.out.println();
		System.out.println("Which space would you like to reserve (enter 0 to cancle)?");
		System.out.println();
		return in.nextLine();
	}

	private String numberAttending() {
		System.out.println("How many people will be in attendance?");
		return in.nextLine();
	}

	private String numberOfDaysReserved() {
		System.out.println("How many days will you need the space?");
		return in.nextLine();
	}

	private String reservationStartDate() {
		System.out.println("When do you need the space?");
		return in.nextLine();
	}

	private String venueDetailMenu(Venue venue) {
		System.out.println();
		System.out.println("--------------------------------------------------------------------"
				+ "------------------------------------------------------------------------------");
		System.out.println(venue.getName() );
		System.out.println("Location: " +venue.getCityName() +", " + venue.getStateAbbrevation() );
		System.out.println();
		System.out.println(venue.getDescription() );
		System.out.println();
		System.out.println("What would you like to do next?");
		System.out.println(VENUE_DETAIL_MENU_OPTION_TO_VIEW_SPACES + ") View Spaces");
		System.out.println(VENUE_DETAIL_MENU_OPTION_TO_SEARCH_FOR_RESERVATION + ") Search for Reservation");
		System.out.println("R) Return to Previous Screen");
		System.out.println();
		System.out.print("Please make a selection >> " );	
		return in.nextLine();
	}

	private String venueMenu(List<Venue> venues) {
		listVenues(venues);
		System.out.println("R) Return to Previous Screen");
		System.out.println();
		System.out.print("Please select a Venue >> " );
		return in.nextLine();	
	}

	private void listVenues(List<Venue> venues) {
		System.out.println();
		if(venues.size() > 0) {
			for (Venue venue : venues) {
				System.out.println(venue.getId() + ") " + venue.getName() );
			}
		}
	}

	public String spaceMenu(List<Space> spaces) {
		System.out.println(spaces.get(0).getName() + "Spaces");
		System.out.println();
		System.out.printf(" %-4s %-35s %-15s %-20s %-20s %-20s", "", "Name", "Open", "Closed", "Daily Rate", "Max. Occupancy");
		System.out.println();
		listSpaces(spaces);
		System.out.println();
		System.out.println("What would you like to do next?");
		System.out.println("   " + SPACE_MENU_OPTION_TO_RESERVE_A_SPACE + ") Reserve a Space");
		System.out.println("   " + RETURN_TO_PREVIOUS_SCREEN_OPTION + ") Return to Previous Screen");
		System.out.println();
		System.out.print("Please make a selection >> " );
		return in.nextLine();
	}

	public String searchForReservationMenu(List<Space> space) {
		System.out.println();
		System.out.println("What would you like to do?");
		System.out.println("   " + RESERVATION_MENU_OPTION_LIST_VENUE + ") List Venues");
		System.out.println("   " + RESERVATION_MENU_DISPLAY_RESERVATION + ") Display Reservation");
		System.out.println("   " + RESERVATION_MENU_QUIT + ") Quit");
		System.out.println();
		System.out.print("Please make a selection >> " );
		return in.nextLine();
	}

	private void listSpaces(List<Space> spaces) {
		for (Space space: spaces) {
			/*			System.out.println("--------------------------------------
			---------------------------------------------------------------------------------------");*/
			System.out.printf("%-4s %-35s %-15s %-20s %-20s %-20s%n", "#" +space.getId(), space.getName(), 
					getMonths(space.getOpen_from()), getMonths(space.getOpen_to()), "$" 
							+ space.getDaily_rate(), space.getMax_occupancy() );
		}
	}

	private void listOfAvaliableSpaces(List<Space> spaces, int numberOfDays) {
		for (Space space: spaces) {
			System.out.printf("%-10s %-25s %-20s %-10s %-15s %-10s%n", space.getId(), 
					space.getName(), "$" + space.getDaily_rate(), space.getMax_occupancy(),
					space.is_accessible(), "$" + (space.getDaily_rate().intValue() * numberOfDays) ) ;
		}
	}

	public String getMonths(long month) {
		if (month == 0) {
			return "";
		} else 
			return new DateFormatSymbols().getMonths()[(int) (month - 1)];
	}	
}

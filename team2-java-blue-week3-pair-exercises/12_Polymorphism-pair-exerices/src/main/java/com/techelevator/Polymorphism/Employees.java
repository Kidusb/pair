package com.techelevator.Polymorphism;

public class Employees implements Worker {

	private String firstName;
	private String lastName;
	private double hourlyRate;
	
	public Employees(String firstName, String lastName, double hourlyRate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.hourlyRate = hourlyRate;
	}
	
	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public double calculateWeeklyPay(int hoursWorked) {
		return calculateWeeklyPay(hoursWorked);
	}
	
	

}

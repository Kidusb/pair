package com.techelevator.Polymorphism;

public class VolunteerWorker extends Employees implements Worker {

	public VolunteerWorker(String firstName, String lastName) {
		super(firstName, lastName, 0);

	}
	
	public double calculateWeeklyPay(int hoursWorked) {
		double pay = hoursWorked * 0;
		return pay;
	}

}
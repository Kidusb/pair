package com.techelevator.Polymorphism;

public class HourlyWorker extends Employees {
	
	private double hourlyRate;
	public double getHourlyRate() {
		return hourlyRate;
	}
	
	HourlyWorker(String firstName, String lastName, double hourlyRate) {
		super(firstName, lastName, hourlyRate);
		this.hourlyRate = hourlyRate;
	}

	@Override
	public double calculateWeeklyPay(int hoursWorked) {
		double pay = hourlyRate * hoursWorked;
		if (hoursWorked > 40) {
			int overtime = hoursWorked - 40;
			pay = pay + (hourlyRate * overtime * .5);
		}
		return pay;
	}
	
	
	

}

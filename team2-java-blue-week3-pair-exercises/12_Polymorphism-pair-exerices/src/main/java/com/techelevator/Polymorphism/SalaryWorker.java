package com.techelevator.Polymorphism;

public class SalaryWorker extends Employees implements Worker {

	private double annualSalary;

	public double getAnnualSalary() {
		return annualSalary;
	}
	
	SalaryWorker(String firstName, String lastName, double annualSalary) {
		super(firstName, lastName, annualSalary);
		this.annualSalary = annualSalary; 
	}

	@Override
	public double calculateWeeklyPay(int hoursWorked) {
		double pay = annualSalary / 52d;
		return pay;
	}


}

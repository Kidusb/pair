package com.techelevator.Polymorphism;

import java.text.DecimalFormat;
import java.util.Random;

public class Program {

	public static void main(String[] args) {
		
		Random hours = new Random();
		
		DecimalFormat df = new DecimalFormat("#0.00");
		
		Worker[] workers = new Worker[] {new HourlyWorker("Tim", "Tom", 5), new HourlyWorker("John", "Doe", 30), new SalaryWorker("Jane", "Doe", 50000), new VolunteerWorker("Bob", "Tom") };
		
		int runningTotalHours = 0;
		double runningTotalPay = 0;
		
		
		System.out.println("Employee             Hours Worked           Pay");
		System.out.println("====================================================");
		
		for(Worker worker : workers) {
			int hoursWorked = hours();
			double calculatedPay = worker.calculateWeeklyPay(hoursWorked);
			System.out.printf("%-1s, %-15s   %-20s$%-15s%n", worker.getLastName(), worker.getFirstName(), hoursWorked, df.format(calculatedPay) );
			runningTotalHours += hoursWorked;
			runningTotalPay += calculatedPay;
		}
		

		System.out.println();
		System.out.println("Total Hours: " + runningTotalHours);
		System.out.println("Total Pay: " +  "$" + df.format(runningTotalPay) );
		}
	
	public static int hours() {
		Random r = new Random();
			return r.nextInt((80 - 0) + 1) + 10;
	}
	
}

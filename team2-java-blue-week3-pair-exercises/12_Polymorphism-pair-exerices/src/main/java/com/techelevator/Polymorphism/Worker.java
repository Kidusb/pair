package com.techelevator.Polymorphism;

public interface Worker {
	
	String getFirstName();
	String getLastName();
	
	double calculateWeeklyPay(int hoursWorked);

}

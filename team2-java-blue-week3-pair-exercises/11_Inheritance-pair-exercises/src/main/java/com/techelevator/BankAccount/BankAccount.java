package com.techelevator.BankAccount;

import java.math.BigDecimal;

public abstract class BankAccount {
	
	private String accountNumber;
	private BigDecimal balance = new BigDecimal("0.00");
	
	public BankAccount() {
		balance = new BigDecimal ("0.00");
		accountNumber = "unknown";		
	}
	
	public BankAccount (String accountNumber, BigDecimal balance) {
		this.accountNumber = accountNumber;
		this.balance = balance;
	}	
	
	//getters & setters
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}
	
	public String toString () {
		return accountNumber;
	}

	//methods	
	public BigDecimal deposit(BigDecimal amountToDeposit) {
		if (amountToDeposit.compareTo(new BigDecimal("0.00")) <= 0) {
			System.out.println("Deposit failed. Deposit must be a positive amount.");
			return balance;
		} else {
			balance = balance.add(amountToDeposit);
			return balance;
		}
		
	}
	
	public BigDecimal withdraw(BigDecimal amountToWithdraw) {
		balance = balance.subtract(amountToWithdraw);
		return balance;
	}
	
	public void transfer(BankAccount destinationAccount, BigDecimal transferAmount) {
		if (balance.compareTo(transferAmount) >= 0) {
			withdraw(transferAmount);
			destinationAccount.deposit(transferAmount);
		}
	}
	
}

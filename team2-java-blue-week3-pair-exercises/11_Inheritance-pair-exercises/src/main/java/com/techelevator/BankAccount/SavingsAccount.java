package com.techelevator.BankAccount;

import java.math.BigDecimal;

public class SavingsAccount extends BankAccount{
	
		public SavingsAccount(String account, BigDecimal balance) {
			super(account, balance);
		}
		
		public SavingsAccount() {
			super();
		}
		
		@Override
		public BigDecimal withdraw(BigDecimal amountToWithdraw) {
			if (amountToWithdraw.compareTo(new BigDecimal("0.00")) <= 0) {
				System.out.println("Withdraw failed. Withdraw must be a positive amount.");
				return super.getBalance();
			} 
			if (super.getBalance().subtract(amountToWithdraw).compareTo(BigDecimal.valueOf(150)) < 0) {
				if (super.getBalance().subtract(amountToWithdraw).compareTo(BigDecimal.valueOf(0)) < 0) {
					System.out.println("It Failed");
					return super.getBalance();
				} else {
				return super.withdraw(amountToWithdraw ).subtract(new BigDecimal("2"));
				}
			} else {
				return super.withdraw(amountToWithdraw );
			}
		}
	}

package com.techelevator.BankAccount;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BankCustomer {
	
		public String name;
		public String address;
		public String phoneNumber;
		public List<BankAccount> accounts = new ArrayList <BankAccount> ();
				
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public List<BankAccount> getAccounts() {
			return accounts;
		}
		
		public void addAccount(BankAccount newAccount) {			
			accounts.add(newAccount);
		}
		
		public boolean isVIP() {
			BigDecimal total = new BigDecimal("0");
			for(BankAccount eachAccount : accounts) {
				total = total.add(eachAccount.getBalance());
			}
			if (total.doubleValue() >= 25000) {
				return true;
			}
			System.out.println(total);
			return false;
		}
}

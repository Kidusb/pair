package com.techelevator.BankAccount;

import java.math.BigDecimal;

public class CheckingAccount extends BankAccount {
	
	public CheckingAccount() {
		super();
	}
	
	public CheckingAccount(String accountNumber, BigDecimal balance) {
		super(accountNumber, balance);
	}
	
	@Override
	public BigDecimal withdraw(BigDecimal amountToWithdraw ) {
		
		if (amountToWithdraw.compareTo(new BigDecimal("0.00")) <= 0) {
			System.out.println("Withdraw failed. Withdraw must be a positive amount.");
			return super.getBalance();
		} 
		if (super.getBalance().subtract(amountToWithdraw).compareTo(BigDecimal.valueOf(0))==-1) {
			if (super.getBalance().subtract(amountToWithdraw ).compareTo(BigDecimal.valueOf(-100)) < 0) {
				System.out.println("It Failed");
				return super.getBalance();
			} else {
			return super.withdraw(amountToWithdraw ).subtract(new BigDecimal("10"));
			}
		} else {
			return super.withdraw(amountToWithdraw );
		}
	}
}
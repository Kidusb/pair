package com.techelevator.BankAccount;

import java.math.BigDecimal;

import com.techelevator.BankAccount.BankAccount;
import com.techelevator.BankAccount.SavingsAccount;

public class BankTeller {

	public static void main(String[] args) {
		
		BankAccount checkingAccount = new CheckingAccount("1234", new BigDecimal ("100") );
		BankAccount savingsAccount = new SavingsAccount("2345", new BigDecimal ("100") );

		BankCustomer jayGatsby = new BankCustomer();
		jayGatsby.addAccount(checkingAccount);
		jayGatsby.addAccount(savingsAccount);

		System.out.println(String.format("Jay Gatsby has %s accounts.", jayGatsby.getAccounts().size())); 
	
		BankAccount aCheckingAccount = new CheckingAccount();
		BankAccount anotherCheckingAccount = new CheckingAccount("123432543", new BigDecimal("18.45"));

		BankAccount aSavingsAccount = new SavingsAccount();
		BankAccount anotherSavingsAccount = new SavingsAccount("1237772543", new BigDecimal("18.45"));

		System.out.println(aCheckingAccount.getBalance());
		System.out.println(anotherCheckingAccount.getBalance());
		System.out.println(aSavingsAccount.getBalance());
		System.out.println(anotherSavingsAccount.getBalance());
		
		BigDecimal amountToDeposit = new BigDecimal("1.00");
		BigDecimal newBalance = anotherCheckingAccount.deposit(amountToDeposit);
		
		System.out.println(anotherCheckingAccount.getBalance());
		
		
		BigDecimal amountToTransfer = new BigDecimal("0.50");
		anotherCheckingAccount.transfer(aCheckingAccount, amountToTransfer);
		
		System.out.println(aCheckingAccount.getBalance());
		
		BigDecimal amountToWithdraw = new BigDecimal ("20");
		BigDecimal newBalance2 = anotherCheckingAccount.withdraw(amountToWithdraw);
		System.out.println(anotherCheckingAccount.getBalance());
		
		BigDecimal newBalance3 = anotherSavingsAccount.withdraw(amountToWithdraw);
		System.out.println(anotherSavingsAccount.getBalance());

		BigDecimal amountToWithdraw2 = new BigDecimal ("10");
//		BigDecimal newBalance21 =
		anotherCheckingAccount.withdraw(amountToWithdraw2);
		System.out.println(anotherCheckingAccount.getBalance());
		
		BigDecimal newBalance31 = anotherSavingsAccount.withdraw(amountToWithdraw2);
		System.out.println(anotherSavingsAccount.getBalance());
		
		BankAccount checkingAccount3 = new CheckingAccount("1324", new BigDecimal ("20000") );
		BankAccount savingsAccount3 = new SavingsAccount("2435", new BigDecimal ("20000") );

		BankCustomer bobGatsby = new BankCustomer();
		bobGatsby.addAccount(checkingAccount3);
		bobGatsby.addAccount(savingsAccount3);

		System.out.println(String.format("Bob Gatsby has %s accounts.", bobGatsby.getAccounts().size())); 
		if(bobGatsby.isVIP()) {
			System.out.println("Bob Gatsby is super VIP");
		} 
		else {
		System.out.println("Bob Gatsby is not VIP");
		} 
		
		}
}
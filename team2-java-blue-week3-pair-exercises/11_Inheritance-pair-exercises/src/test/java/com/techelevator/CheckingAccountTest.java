package com.techelevator;

import java.math.BigDecimal;

import org.junit.*;

import com.techelevator.BankAccount.CheckingAccount;


public class CheckingAccountTest {
	
	private CheckingAccount checkingAccount;
	
	@Before
	public void setup() {
		checkingAccount = new CheckingAccount();
	}
	
	@Test
	// check if default balance is 0
	public void balance_falls_below_0_with_withdraw_$10_fee() {
		BigDecimal result = checkingAccount.getBalance();
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
//	@Test
	
	
	
	
	
	
	
	
	
	
	
	
	
	// withdraw - falls below $0 - removes extra $10
	// withdraw  - falls below $-100 not allowed (removes nothing
	// withdraw - new balance is old balance minus withdraw amount
	
	// withdraw - negative number shouldn't work
	//deposit - negative number shouldn't work
	// deposit - 0
	// withdraw - 0
	//transfer - 0
	//transfer - negative shouldn't work
	//transfer - 100
	//try transferring too much (more than account balance)
	//retrieve account number
	//set account number
	

}

package com.techelevator.BankAccount;


import java.math.BigDecimal;
import java.util.List;

import org.junit.*;

public class BankCustomerTest {
	
	private BankCustomer bankCustomer;
	private CheckingAccount checkingAccount;
	private CheckingAccount othercheckingAccount;
	
	@Before
	public void setup() {
		bankCustomer = new BankCustomer();
		checkingAccount = new CheckingAccount();
		othercheckingAccount = new CheckingAccount();
		bankCustomer.setName("Kidus R. Ocks");
		bankCustomer.setAddress("123 Best Street Ever");
		bankCustomer.setPhoneNumber("555-555-5555");
		checkingAccount.setAccountNumber("12345");
		othercheckingAccount.setAccountNumber("23456");
		bankCustomer.addAccount(checkingAccount);
		bankCustomer.addAccount(othercheckingAccount);
	}
	
	@Test
	// check name
	public void check_name() {
		String result = bankCustomer.getName();
		Assert.assertEquals("Kidus R. Ocks",  result);
	}
	
	@Test
	// check address
	public void check_address() {
		String result = bankCustomer.getAddress();
		Assert.assertEquals("123 Best Street Ever",  result);
	}
	
	@Test
	// check phone number
	public void check_phone_number() {
		String result = bankCustomer.getPhoneNumber();
		Assert.assertEquals("555-555-5555",  result);
	}
	
	@Test
	// multiple bank accounts (return array)
	public void check_account_list() {
		List<BankAccount> resultList = bankCustomer.getAccounts();
		String[] output = new String[resultList.size()];

		for(int i = 0; i < resultList.size(); i++) {
			output[i] = resultList.get(i).getAccountNumber(); 
		}
		String [] accountListTest = {"12345", "23456"};
		Assert.assertArrayEquals(accountListTest,  output);
	}	

	@Test 
	// isVIP @ 25,000 - true
	public void returns_true_if_25000_in_account() {
		checkingAccount.deposit(new BigDecimal ("25000") );
		boolean results = bankCustomer.isVIP(); 
		Assert.assertTrue(results);
	}
	@Test
	// isVIP @ 24,999 - false
	public void returns_false_if_24999_in_account() {
		checkingAccount.deposit(new BigDecimal ("24999") );
		boolean results = bankCustomer.isVIP(); 
		Assert.assertFalse(results);
	}
	
	@Test
	// isVIP @ -10 - false
	public void returns_false_if_negative_in_account() {
		checkingAccount.withdraw(new BigDecimal ("10") );
		boolean results = bankCustomer.isVIP(); 
		Assert.assertFalse(results);
	}
	@Test 
	// isVIP @ 50,000 - true
	public void returns_true_if_50000_in_account() {
		checkingAccount.deposit(new BigDecimal ("50000") );
		boolean results = bankCustomer.isVIP(); 
		Assert.assertTrue(results);
	}	

}

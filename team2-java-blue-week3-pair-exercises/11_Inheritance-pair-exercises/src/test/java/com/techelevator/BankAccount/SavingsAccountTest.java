package com.techelevator.BankAccount;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SavingsAccountTest {
	
	private SavingsAccount savingsAccount;
	private SavingsAccount othersavingsAccount;

	
	@Before
	public void setup() {
		savingsAccount = new SavingsAccount();
	}
	
	@Test
	// check if default balance is 0
	public void check_default_balance_0() {
		BigDecimal result = savingsAccount.getBalance();
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
	@Test
	// deposit - normal
	public void balance_100_with_100_deposit() {
		BigDecimal result = savingsAccount.deposit(new BigDecimal("100"));
		Assert.assertEquals(new BigDecimal("100.00"),  result);
	}
	
	@Test
	// deposit - 0
	public void balance_0_with_0_deposit() {
		BigDecimal deposit = new BigDecimal("0");
		BigDecimal result = savingsAccount.getBalance().add(deposit);
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
	@Test
	//deposit - negative number shouldn't work
	public void balance_0_with_negative_deposit() {
		BigDecimal deposit = new BigDecimal("-100.00");
		BigDecimal result = savingsAccount.deposit(deposit);
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
	@Test
	// withdraw - negative number shouldn't work
	public void balance_0_with_negative_withdraw() {
		BigDecimal withdraw = new BigDecimal("-100.00");
		BigDecimal result = savingsAccount.withdraw(withdraw);
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
	@Test
	// withdraw - normal
	public void balance_900_with_1000_deposit_100_withdraw() {
		BigDecimal deposit = new BigDecimal("1000.00");
		BigDecimal withdraw = new BigDecimal("100.00");
		BigDecimal result = savingsAccount.deposit(deposit);
		result = savingsAccount.withdraw(withdraw);
		Assert.assertEquals(new BigDecimal("900.00"),  result);
	}
	
	@Test
	// withdraw - falls below $150 - removes extra $2
	public void balance_1000_withdraw_900_fee_2_return_neg_98() {
		BigDecimal deposit = new BigDecimal("1000.00");
		BigDecimal withdraw = new BigDecimal("900.00");
		BigDecimal result = savingsAccount.deposit(deposit);
		result = savingsAccount.withdraw(withdraw);
		Assert.assertEquals(new BigDecimal("98.00"),  result);
	}
	
	@Test
	// withdraw  - falls below $0 not allowed (removes nothing)
	public void balance_1000_withdraw_12000_not_allowed_return_1000() {
		BigDecimal deposit = new BigDecimal("1000.00");
		BigDecimal withdraw = new BigDecimal("1200.00");
		BigDecimal result = savingsAccount.deposit(deposit);
		result = savingsAccount.withdraw(withdraw);
		Assert.assertEquals(new BigDecimal("1000.00"),  result);
	}
	
	@Test
	// withdraw - 0
	public void balance_0_withdraw_0_return_0() {
		BigDecimal withdraw = new BigDecimal("0.00");
		BigDecimal result = savingsAccount.withdraw(withdraw);
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
	@Test
	//transfer - 0
	public void balance_0_transfer_0_return_0() {
		othersavingsAccount = new SavingsAccount("1234", new BigDecimal ("0") );
		BigDecimal transfer = new BigDecimal("0.00");
//		BigDecimal result = checkingAccount.withdraw(transfer);
		savingsAccount.transfer(othersavingsAccount, transfer);
		BigDecimal result = savingsAccount.getBalance();
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	@Test
	//transfer - 80
	public void balance_1000_transfer_80_return_920() {
		othersavingsAccount = new SavingsAccount("1234", new BigDecimal ("0") );
		BigDecimal deposit = new BigDecimal("1000.00");
		BigDecimal result = savingsAccount.deposit(deposit);
		BigDecimal transfer = new BigDecimal("80.00");
		savingsAccount.transfer(othersavingsAccount, transfer);
		result = savingsAccount.getBalance();
		Assert.assertEquals(new BigDecimal("920.00"),  result);
	}
	
	@Test
	//transfer - negative shouldn't work
	public void balance_1000_transfer_negitive_80_return_1000_not_work() {
		othersavingsAccount = new SavingsAccount("1234", new BigDecimal ("0") );
		BigDecimal deposit = new BigDecimal("1000.00");
		BigDecimal result = savingsAccount.deposit(deposit);
		BigDecimal transfer = new BigDecimal("-80.00");
		savingsAccount.transfer(othersavingsAccount, transfer);
		result = savingsAccount.getBalance();
		Assert.assertEquals(new BigDecimal("1000.00"),  result);
	}
	@Test
	//try transferring too much (more than account balance)
	public void balance_1000_transfer_1200_return_1000() {
		othersavingsAccount = new SavingsAccount("1234", new BigDecimal ("0") );
		BigDecimal deposit = new BigDecimal("1000.00");
		BigDecimal result = savingsAccount.deposit(deposit);
		BigDecimal transfer = new BigDecimal("1200.00");
		savingsAccount.transfer(othersavingsAccount, transfer);
		result = savingsAccount.getBalance();
		Assert.assertEquals(new BigDecimal("1000.00"),  result);
	}
	
	@Test
	//retrieve account number
	public void set_and_check_account_number() {
		othersavingsAccount = new SavingsAccount("1234", new BigDecimal ("0") );
		String result = othersavingsAccount.getAccountNumber();
		Assert.assertEquals("1234",  result);
	}
	
	
	

}

package com.techelevator.BankAccount;

import java.math.BigDecimal;

import org.junit.*;

import com.techelevator.BankAccount.BankAccount;

public class BankAccountTest {

private BankAccount bankAccount;

	@Before
	public void setup() {
		bankAccount = new BankAccount();
	}
	
	@Test
	public void testing_if_deposit_works() {
		BigDecimal deposit = new BigDecimal(10.00);
		BigDecimal balance = bankAccount.getBalance().add(deposit);
		Assert.assertEquals(new BigDecimal(10), balance);
		
		//		BigDecimal n = new BigDecimal(10); 
//				BigDecimal balance = BankAccount.balance.add(amountToDeposit));
//				Assert.assertEquals(new BigDecimal(10), balance);
	
	}
}


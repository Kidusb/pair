package com.techelevator.BankAccount;

import java.math.BigDecimal;

import org.junit.*;

import com.techelevator.BankAccount.CheckingAccount;

public class CheckingAccountTest {
	
	private CheckingAccount checkingAccount;
	private CheckingAccount othercheckingAccount;

	
	@Before
	public void setup() {
		checkingAccount = new CheckingAccount();
	}
	
	@Test
	// check if default balance is 0
	public void check_default_balance_0() {
		BigDecimal result = checkingAccount.getBalance();
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
	@Test
	// deposit - normal
	public void balance_100_with_100_deposit() {
		BigDecimal result = checkingAccount.deposit(new BigDecimal("100"));
		Assert.assertEquals(new BigDecimal("100.00"),  result);
	}
	
	@Test
	// deposit - 0
	public void balance_0_with_0_deposit() {
		BigDecimal deposit = new BigDecimal("0");
		BigDecimal result = checkingAccount.getBalance().add(deposit);
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
	@Test
	//deposit - negative number shouldn't work
	public void balance_0_with_negative_deposit() {
		BigDecimal deposit = new BigDecimal("-100.00");
		BigDecimal result = checkingAccount.deposit(deposit);
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
	@Test
	// withdraw - negative number shouldn't work
	public void balance_0_with_negative_withdraw() {
		BigDecimal withdraw = new BigDecimal("-100.00");
		BigDecimal result = checkingAccount.withdraw(withdraw);
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
	@Test
	// withdraw - normal
	public void balance_900_with_1000_deposit_100_withdraw() {
		BigDecimal deposit = new BigDecimal("1000.00");
		BigDecimal withdraw = new BigDecimal("100.00");
		BigDecimal result = checkingAccount.deposit(deposit);
		result = checkingAccount.withdraw(withdraw);
		Assert.assertEquals(new BigDecimal("900.00"),  result);
	}
	
	@Test
	// withdraw - falls below $0 - removes extra $10
	public void balance_0_withdraw_10_fee_10_return_neg_20() {
		BigDecimal withdraw = new BigDecimal("10.00");
		BigDecimal result = checkingAccount.withdraw(withdraw);
		Assert.assertEquals(new BigDecimal("-20.00"),  result);
	}
	
	@Test
	// withdraw  - falls below $-100 not allowed (removes nothing)
	public void balance_0_withdraw_200_not_allowed_return_0() {
		BigDecimal withdraw = new BigDecimal("200.00");
		BigDecimal result = checkingAccount.withdraw(withdraw);
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
	@Test
	// withdraw - 0
	public void balance_0_withdraw_0_return_0() {
		BigDecimal withdraw = new BigDecimal("0.00");
		BigDecimal result = checkingAccount.withdraw(withdraw);
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	
	@Test
	//transfer - 0
	public void balance_0_transfer_0_return_0() {
		othercheckingAccount = new CheckingAccount("1234", new BigDecimal ("0") );
		BigDecimal transfer = new BigDecimal("0.00");
//		BigDecimal result = checkingAccount.withdraw(transfer);
		checkingAccount.transfer(othercheckingAccount, transfer);
		BigDecimal result = checkingAccount.getBalance();
		Assert.assertEquals(new BigDecimal("0.00"),  result);
	}
	@Test
	//transfer - 80
	public void balance_1000_transfer_80_return_920() {
		othercheckingAccount = new CheckingAccount("1234", new BigDecimal ("0") );
		BigDecimal deposit = new BigDecimal("1000.00");
		BigDecimal result = checkingAccount.deposit(deposit);
		BigDecimal transfer = new BigDecimal("80.00");
		checkingAccount.transfer(othercheckingAccount, transfer);
		result = checkingAccount.getBalance();
		Assert.assertEquals(new BigDecimal("920.00"),  result);
	}
	
	@Test
	//transfer - negative shouldn't work
	public void balance_1000_transfer_negitive_80_return_1000_not_work() {
		othercheckingAccount = new CheckingAccount("1234", new BigDecimal ("0") );
		BigDecimal deposit = new BigDecimal("1000.00");
		BigDecimal result = checkingAccount.deposit(deposit);
		BigDecimal transfer = new BigDecimal("-80.00");
		checkingAccount.transfer(othercheckingAccount, transfer);
		result = checkingAccount.getBalance();
		Assert.assertEquals(new BigDecimal("1000.00"),  result);
	}
	@Test
	//try transferring too much (more than account balance)
	public void balance_1000_transfer_1200_return_1000() {
		othercheckingAccount = new CheckingAccount("1234", new BigDecimal ("0") );
		BigDecimal deposit = new BigDecimal("1000.00");
		BigDecimal result = checkingAccount.deposit(deposit);
		BigDecimal transfer = new BigDecimal("1200.00");
		checkingAccount.transfer(othercheckingAccount, transfer);
		result = checkingAccount.getBalance();
		Assert.assertEquals(new BigDecimal("1000.00"),  result);
	}
	
	@Test
	//retrieve account number
	public void set_and_check_account_number() {
		othercheckingAccount = new CheckingAccount("1234", new BigDecimal ("0") );
		String result = othercheckingAccount.getAccountNumber();
		Assert.assertEquals("1234",  result);
	}
	
	
	

}

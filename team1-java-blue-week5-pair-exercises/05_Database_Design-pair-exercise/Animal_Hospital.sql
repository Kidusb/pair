--Animal hospital
DROP TABLE IF EXISTS pet_owner; 
DROP TABLE IF EXISTS address; 
DROP TABLE IF EXISTS owner; 
DROP TABLE IF EXISTS invoice; 
DROP TABLE IF EXISTS procedure;
DROP TABLE IF EXISTS pet;  

CREATE TABLE owner (
        owner_id serial PRIMARY KEY,
        first_name varchar (35) NOT NULL,
        last_name varchar (35) NOT NULL
        );
        
CREATE TABLE address (
        address_id serial PRIMARY KEY,
        owner_id int, 
        street_line_one varchar(64) NOT NULL,
        street_line_two varchar(64),
        city varchar(128) NOT NULL,
        district varchar(128) NOT NULL,
        postalcode varchar(20) NOT NULL,
        country char(3) DEFAULT 'USA' NOT NULL,
        type varchar(15) NOT NULL,
        
        constraint fk_owner_address_id foreign key (owner_id) references owner(owner_id)
        );
                
 CREATE TABLE pet(
        pet_id serial primary key,
        pet_name varchar(64) NOT NULL
        );
        
CREATE TABLE pet_owner (
        owner_id INT,
        pet_id INT,
               
        primary key(owner_id, pet_id),
        constraint fk_pet_owner_id foreign key (owner_id) references owner(owner_id),
        constraint fk_pet_id foreign key (pet_id) references pet(pet_id)
        );
        
CREATE TABLE procedure (
        procedure_id serial primary key,
        pet_id int,
        procedure_type varchar (30),
        procedure_date date,
        price decimal (5, 2),
        
        constraint fk_pet_procedure_id foreign key (pet_id) references pet(pet_id)
        );
        
CREATE TABLE invoice (
        invoice_id serial primary key,
        procedure_id int,
        
        constraint fk_invoice_procedure_id foreign key (procedure_id) references procedure(procedure_id)
        );

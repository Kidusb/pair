package com.techelevator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class WordCount {

	public static void main(String[] args) {
		
		Menu menu = new Menu();
		
		File userfile = menu.getFile();
		
		Counter counter = new Counter(userfile);
		
		int wordCount = 0;
		
		int sentenceCount = 0;

		try {
			wordCount = counter.wordCounter(userfile);
			sentenceCount = counter.sentenceCounter(userfile);
		} catch (FileNotFoundException e) {
			menu.displayMessage("File not found: " + userfile.getAbsolutePath() );
		} catch (Exception e) {
			menu.displayMessage("Unknown error occurred: " + e.getMessage() );
		}
		
		
		menu.displayMessage("Word count: " + wordCount);
		menu.displayMessage("Sentence count: " + sentenceCount);

	}

}

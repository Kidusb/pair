package com.techelevator;

import java.io.File;
import java.util.Scanner;

public class Menu {
	
	private Scanner in = new Scanner(System.in);
	
	public File getFile() {
		System.out.println("What file would you like to search? ");
		String filepath = in.nextLine();
		File userfile = new File (filepath);
		return userfile;
	}

	public void displayMessage(String message) {	
		System.out.println(message);
	}
	
}

package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

public class Counter {

	private File userfile;
	
	public Counter(File userfile) {
		this.userfile = userfile;
		
	}
	
	public int wordCounter (File userfile) throws FileNotFoundException {
		int wordCount = 0;
		
		try (Scanner fileScanner = new Scanner(this.userfile) ) {
			while(fileScanner.hasNextLine() ) {
				String line = fileScanner.nextLine();
				String[] wordsInLine = line.split("\\b+");
				wordCount += (wordsInLine.length / 2);
			}
	}
		

		return wordCount;
	}

	public int sentenceCounter (File userfile) throws FileNotFoundException {
		int sentenceCount = 0;
		
		try (Scanner fileScanner = new Scanner(this.userfile) ) {
			while(fileScanner.hasNextLine() ) {
				String line = fileScanner.nextLine();
								
				String[] sentences = line.split("\\.|\\!|\\?");
			
				sentenceCount += sentences.length - 1;
			}
		}
			
		return sentenceCount;
		
	}
	
}


package com.techelevator.view;

import java.math.BigDecimal;
import java.util.Map;

import org.junit.*;

import com.techelevator.cashflow.Register;
import com.techelevator.inventory.Appetizer;
import com.techelevator.inventory.Beverage;
import com.techelevator.inventory.Cart;
import com.techelevator.inventory.Item;

public class MenuTest {

	private Menu menu;
	
	@Before 
	public void setup() {
		menu = new Menu();
	}
	
	@Test
	public void display_report_outputs_as_intended() {
		
	Cart cart = new Cart();
	
	Item[] testItems = { new Beverage("Pop" , new BigDecimal("2.00")) , new Appetizer("Fries" , new BigDecimal("1.00"))};
	
	for (int i = 0 ; i < testItems.length ; i++) {
		cart.addItemToCart(testItems[i], 2);
	}
	
	menu.displayReport(cart);
	
	}
	
	@Test
	public void display_change_outputs_as_expected() {
		Register register = new Register();
		register.deposit(new BigDecimal("27.80"));
		
		Map<String, Integer> change = register.giveChange();
		menu.displayChange(change, register.getBalance());
	}
	
}

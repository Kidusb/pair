package com.techelevator.fileio;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.*;

import com.techelevator.inventory.Appetizer;
import com.techelevator.inventory.Beverage;
import com.techelevator.inventory.Dessert;
import com.techelevator.inventory.Entree;
import com.techelevator.inventory.Item;
import com.techelevator.inventory.Listing;


public class InventoryFileReaderTest {
	private static final String INVENTORY_FILE = "cateringsystem.csv";

	private InventoryFileReader reader; 

	@Before
	public void setup() {
		File file = new File(INVENTORY_FILE);
		reader = new InventoryFileReader(file);
	}

	@Test
	public void inventory_has_right_size() {

		int expectedInventorySize = 18;
		Map<String, Listing> inventory = reader.stock();
		int inventorySize = inventory.size();
		
		Assert.assertNotNull(inventorySize);
		Assert.assertEquals(expectedInventorySize, inventorySize);		
	}
	
	@Test
	public void inventory_builds_items_correctly() {
		Map<String, Listing> inventory = reader.stock();

		String[] testKeys = {"A1" , "B2" , "E3" , "D4"};
		
		Item[] testValues = {new Appetizer("Tropical Fruit Bowl" , new BigDecimal("3.50")) ,
				new Beverage("Wine" , new BigDecimal("3.05")) ,
				new Entree("BBQ Ribs" , new BigDecimal("11.65")) ,
				new Dessert("Jolly Ranger Tart" , new BigDecimal("0.85"))};
		
		for (int i = 0 ; i < testKeys.length ; i++) {
			Item testItem = testValues[i];
			String key = testKeys[i];
			
			Assert.assertNotNull(inventory.get(key));
			Assert.assertEquals(testItem.getName(), inventory.get(key).getItemName());
			Assert.assertEquals(testItem.getPrice(), inventory.get(key).getItemPrice());
		}
	}
}

	

package com.techelevator.cashflow;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.*;

public class RegisterTest {

	private Register register;
	
	@Before 
	public void setup() {
		register = new Register();
	}
	
	@Test
	public void balance_0_deposit_50_updated_balance_50() {
		register.deposit(new BigDecimal("50.00"));
		
		Assert.assertEquals(new BigDecimal("50.00") , register.getBalance());
	}
	
	@Test
	public void balance_2500_deposit_2500_updated_balance_5000() {
		register.deposit(new BigDecimal("2500.00"));
		register.deposit(new BigDecimal("2500.00"));

		Assert.assertEquals(new BigDecimal("5000.00") , register.getBalance());
	}
	
	@Test
	public void balance_4500_deposit_500_balance_5000() {
		register.deposit(new BigDecimal("4500.00"));
		register.deposit(new BigDecimal("500.00"));

		Assert.assertEquals(new BigDecimal("5000.00"), register.getBalance());

	}
	
	@Test
	public void balance_4500_deposit_501_balance_4500() {
		register.deposit(new BigDecimal("4500.00"));
		register.deposit(new BigDecimal("501.00"));

		Assert.assertEquals(new BigDecimal("4500.00"), register.getBalance());

	}
	
	@Test
	public void balance_4999_deposit_2_balance_unchanged() {
		register.deposit(new BigDecimal("4999.00"));
		register.deposit(new BigDecimal("2.00"));

		Assert.assertEquals(new BigDecimal("4999.00") , register.getBalance());
	}
	
	
	@Test
	public void balance_5000_deposit_1_balance_unchanged() {
		register.deposit(new BigDecimal("5000.00"));
		register.deposit(new BigDecimal("1.00"));

		Assert.assertEquals(new BigDecimal("5000.00") , register.getBalance());
	}
	
	@Test
	public void balance_0_charge_50_balance_unchanged() {
		register.charge(new BigDecimal("50.00"));
		
		Assert.assertEquals(new BigDecimal("0.00") , register.getBalance());
	}
	
	@Test
	public void balance_10_charge_11_balance_10() {
		register.deposit(new BigDecimal("10.00"));
		register.charge(new BigDecimal("11.00"));
		
		Assert.assertEquals(new BigDecimal("10.00") , register.getBalance());
	}
	
	@Test
	public void balance_50_charge_25_balance_25() {
		register.deposit(new BigDecimal("50.00"));
		register.charge(new BigDecimal("25.00"));
		
		Assert.assertEquals(new BigDecimal("25.00") , register.getBalance());
	}
	
	@Test
	public void balance_4500_charge_500_balance_4000() {
		register.deposit(new BigDecimal("4500.00"));
		register.charge(new BigDecimal("500.00"));
		
		Assert.assertEquals(new BigDecimal("4000.00"), register.getBalance());
	}
	
	@Test
	public void balance_50_dollars_and_30_cents_giveChange_2_20s_1_10_1_quarter_1_nickel() {
		register.deposit(new BigDecimal("50.30"));
		
		Map<String, Integer> changeCount = register.giveChange();
		
		String change = "Your change is: ";
		
		for (Entry<String, Integer> entry : changeCount.entrySet()) {
			Integer value = entry.getValue();
			String key = (value > 1) ? entry.getKey() + "s" : entry.getKey();

			change = change + value + " " + key + " ";
		}
		
		Assert.assertEquals("Your change is: 2 $20 bills 1 $10 bill 0 $5 bill 0 $1 bill 1 quarter 0 dime 1 nickel ", change);
	}
	
}

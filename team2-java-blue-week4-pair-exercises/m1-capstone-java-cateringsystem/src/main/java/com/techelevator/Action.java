package com.techelevator;

import java.util.Date;

public class Action {
	
 	private String action;
 	private Date date;
	
 	public Action (String action) {
 		this.action = action;
 		this.date = new Date();
 	}

	public String getAction() {
		return action;
	}

	public Date getDate() {
		return date;
	}
}

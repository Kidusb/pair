package com.techelevator.inventory;

import java.math.BigDecimal;

public class Beverage extends Item {
	
	public Beverage(String name, BigDecimal price) {
		super(name, price, "Beverage");		
	}
}

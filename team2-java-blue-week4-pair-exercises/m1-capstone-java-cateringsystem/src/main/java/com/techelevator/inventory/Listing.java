package com.techelevator.inventory;

import java.math.BigDecimal;

public class Listing {
	
	private Item item;
	private int stockCount; 
	
	public Listing (Item item) {
		this.item = item;
		this.stockCount = 50;
	}
	
	public Item getItem() {
		return item;
	}
	
	public String getItemName() {
		return item.getName();
	}

	public BigDecimal getItemPrice() {
		return item.getPrice();
	}
	
	public String getItemCategory() {
		return item.getCategory();
	}
	
	public int getStockCount() {
		return stockCount;
	}
	
	public void adjustStock(int quantitySold) {
		stockCount -= quantitySold;
	}
}

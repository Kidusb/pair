package com.techelevator.inventory;

import java.math.BigDecimal;

public class Entree extends Item{

	public Entree(String name, BigDecimal price) {
		super(name, price, "Entree");
	}
}

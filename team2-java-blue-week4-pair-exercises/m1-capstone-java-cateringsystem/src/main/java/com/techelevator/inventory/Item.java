package com.techelevator.inventory;

import java.math.BigDecimal;

public class Item {

	private String name;
	private BigDecimal price;
	private String category;

	public Item(String name, BigDecimal price, String category) {
		this.name = name;
		this.price = price;
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getPrice() {
		return price;
	}
	
	public String getCategory() {
		return category;
	}

}

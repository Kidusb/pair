package com.techelevator.inventory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Cart {

	private BigDecimal cartTotal;
	private Map<Item, Integer > itemsInCart = new HashMap<Item, Integer>();

	public Cart() {
		cartTotal = new BigDecimal ("0.00");
	}

	public BigDecimal getCartTotal() {
		return cartTotal;
	}

	public Map<Item, Integer> getItemsInCart() {
		return itemsInCart;
	}

	public int getCartSize() {
		return itemsInCart.size();
	}

	public void addItemToCart(Item item, int quantity) {
		itemsInCart.put(item, quantity);
		BigDecimal price = item.getPrice().multiply(new BigDecimal (quantity) );
		cartTotal = cartTotal.add(price);
	}
}

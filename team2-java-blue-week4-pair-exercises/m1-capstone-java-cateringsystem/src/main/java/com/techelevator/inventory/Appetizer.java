package com.techelevator.inventory;

import java.math.BigDecimal;

public class Appetizer extends Item{
	
	public Appetizer(String name, BigDecimal price) {
		super(name, price, "Appetizer");
	}
}

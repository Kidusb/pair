package com.techelevator.inventory;

import java.math.BigDecimal;

public class Dessert extends Item{
	
	public Dessert(String name, BigDecimal price) {
		super(name, price, "Dessert");
	}
}

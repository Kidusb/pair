package com.techelevator.view;

import java.io.File;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import com.techelevator.inventory.Cart;
import com.techelevator.inventory.Item;
import com.techelevator.inventory.Listing;

public class Menu {
	
	private Scanner in = new Scanner(System.in); 

	public void displayMessage(String message) {
		System.out.println(message);
		System.out.println();
	}
	
	public String getInventoryFilePathFromUser() {
		System.out.println("Please enter the inventory file path: ");	
		String filePath = in.nextLine();
		System.out.println();
		return filePath;
	}
	
	public String mainMenu(String[] options) {
		displayMainMenu(options);
		return getMenuChoice(options);
	}
	
	public String orderMenu(String[] options, BigDecimal balance) {
		displayOrderMenu(options, balance);
		return getMenuChoice(options);
	}
	
	public void displayMainMenu(String[] options) {
		int i = 1;
		for (String option : options) {
			System.out.println( i + ".) " + option);
			i++;
		}
		System.out.println();
	}
	
	public void displayReport(Cart cart) {
		Map<Item, Integer > itemsInCart = cart.getItemsInCart();
		BigDecimal total = cart.getCartTotal();
		
		System.out.printf("%36s", "Transaction Report");
		System.out.println();
		System.out.println("--------------------------------------------------------");

		
		for (Entry<Item, Integer> entry : itemsInCart.entrySet()) {
			Item item = entry.getKey();
			int quantity = entry.getValue();
			BigDecimal totalItemPrice = item.getPrice().multiply(new BigDecimal (quantity));
						
			System.out.printf("%-3s", quantity);
			System.out.printf("%-15s", item.getCategory());
			System.out.printf("%-20s", item.getName());
			System.out.printf("%8s", "$" + item.getPrice());
			System.out.printf("%8s", "$" + totalItemPrice);
			System.out.println();
		}
		
		System.out.println("Total: $" + total);
		System.out.println();
	}
	
	public void displayChange(Map<String, Integer> change, BigDecimal balance) {
		String output = "Change Given: $" + balance + " (";
		int i = 0;
		
		for (Entry<String, Integer> entry : change.entrySet()) {
			if (i <= change.size() - 2) {
				output = output + entry.getValue() + " " + entry.getKey() + ", ";
			} else {
				output = output + entry.getValue() + " " + entry.getKey() + ")";
			}
			i++;
		}
		System.out.println(output);
		System.out.println();
	}
	
	private void displayOrderMenu(String[] options, BigDecimal balance) {
		int i = 1;
		for (String option : options) {
			System.out.println( i + ".) " + option);
			i++;
		}
		System.out.println("Current Account Balance : $" + balance);
		System.out.println();
	}
	
	private String getMenuChoice(String[] options) {
		String userChoice = null;
		while (userChoice == null) {
			
			int userInput = getUserChoice();
			
			if (userInput <= 0 || userInput > options.length + 1) {
				System.out.println(userInput + " is not a menu option. Try again.");
				System.out.println();
			} else {
				userChoice = options[userInput - 1];
			}
		}
		return userChoice;
	}
	
	private int getUserChoice() {
		System.out.println("Please make a selection.");
		int userInput;
		System.out.println();
		
		while (true) {
			System.out.print("Your selection: ");
			String input = in.nextLine();
			System.out.println();
			
			try {
				userInput = Integer.parseInt(input);
				break;
			} catch (NumberFormatException e) {
				System.out.println(input + " is not a valid choice. Try again.");
				System.out.println();
			}
		}
		
		System.out.println();
		
		return userInput;
	}
	
	public BigDecimal getDepositAmount() {
		System.out.println("Please enter a whole number deposit amount: ");
		BigDecimal depositAmount = new BigDecimal(in.nextInt());
		in.nextLine();
		System.out.println();
		return depositAmount;
	}
	
	public String getOrderItem() {
		System.out.println("Please select a product: ");
		String item = in.nextLine();
		item = item.toUpperCase();
		System.out.println();
		return item;
	}
	
	public int getOrderQuantity() {
		System.out.println("How many would you like to order?");
		int quantity = in.nextInt();
		in.nextLine();
		System.out.println();
		return quantity;
	}
	
	public void displayInventory(Map<String, Listing> inventory , boolean inMainMenu) {

		for (Entry<String, Listing> entry : inventory.entrySet()) {
			if (entry.getKey().equalsIgnoreCase("B1")) {
				System.out.println("----------------------------------------------");
				System.out.printf("%26s","Beverages");
				System.out.println();
				System.out.println("----------------------------------------------");
				System.out.printf("%-10s", "Order ID");
				System.out.printf("%-25s", "Product");

				if (inMainMenu) {
					System.out.printf("%-10s", "Stock");
				} else {
					System.out.printf("%-10s", "Price");
				}
				
				System.out.println();
				System.out.println("----------------------------------------------");
			}
			if (entry.getKey().equalsIgnoreCase("A1")) {
				System.out.println("----------------------------------------------");
				System.out.printf("%27s","Appetizers");
				System.out.println();
				System.out.println("----------------------------------------------");
				System.out.printf("%-10s", "Order ID");
				System.out.printf("%-25s", "Product");

				if (inMainMenu) {
					System.out.printf("%-10s", "Stock");
				} else {
					System.out.printf("%-10s", "Price");
				}
				
				System.out.println();
				System.out.println("----------------------------------------------");
			}
			if (entry.getKey().equalsIgnoreCase("E1")) {
				System.out.println("----------------------------------------------");
				System.out.printf("%25s", "Entrees");
				System.out.println();
				System.out.println("----------------------------------------------");
				System.out.printf("%-10s", "Order ID");
				System.out.printf("%-25s", "Product");

				if (inMainMenu) {
					System.out.printf("%-10s", "Stock");
				} else {
					System.out.printf("%-10s", "Price");
				}
				
				System.out.println();
				System.out.println("----------------------------------------------");
			}
			if (entry.getKey().equalsIgnoreCase("D1")) {
				System.out.println("----------------------------------------------");
				System.out.printf("%26s","Desserts");
				System.out.println();
				System.out.println("----------------------------------------------");
				System.out.printf("%-10s", "Order ID");
				System.out.printf("%-25s", "Product");
				
				if (inMainMenu) {
					System.out.printf("%-10s", "Stock");
				} else {
					System.out.printf("%-10s", "Price");
				}
				
				System.out.println();
				System.out.println("----------------------------------------------");
			}

			//System.out.print(entry.getKey() + " " + entry.getValue().getItemName());
			System.out.printf("%-10s", entry.getKey());
			System.out.printf("%-25s", entry.getValue().getItemName());
			
			if (inMainMenu) {
				System.out.printf("%-10s", entry.getValue().getStockCount());
				System.out.println();
			} else {
				System.out.printf("%-10s", entry.getValue().getItemPrice());
				System.out.println();
			}
		}

		System.out.println(); 

	}
}

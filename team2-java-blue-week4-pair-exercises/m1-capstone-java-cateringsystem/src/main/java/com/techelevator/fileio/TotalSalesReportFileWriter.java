package com.techelevator.fileio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Map.Entry;

import com.techelevator.Action;
import com.techelevator.inventory.Item;

public class TotalSalesReportFileWriter implements Writer {

	private static final File TOTAL_SYSTEM_SALES_REPORT = new File("TotalSales.rpt");
	private TotalSalesReport tsr;
	
	public void setTSR(TotalSalesReport tsr) {
		this.tsr = tsr;
	}
	
	@Override
	public void write() throws IOException {		
		try (FileWriter fileWriter = new FileWriter(TOTAL_SYSTEM_SALES_REPORT, TOTAL_SYSTEM_SALES_REPORT.exists());
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
			
			Map<Item, Integer> itemsSold = tsr.getItemsSold();
			
			for (Entry<Item, Integer> entry : itemsSold.entrySet()) {
				BigDecimal itemPrice = entry.getKey().getPrice();
				BigDecimal groupPrice = itemPrice.multiply(new BigDecimal(entry.getValue()));
				bufferedWriter.write(entry.getKey().getName() + "\\|" + entry.getValue() + "\\|$" + groupPrice);
				bufferedWriter.write(System.getProperty("line.separator"));
			}
			
			bufferedWriter.write(System.getProperty("line.separator") + System.getProperty("line.separator"));
			bufferedWriter.write("Total Sales: $" + tsr.getTotalSales());
			bufferedWriter.write(System.getProperty("line.separator"));
		}
	}

}

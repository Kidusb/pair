package com.techelevator.fileio;

import java.util.Map;

import com.techelevator.inventory.Item;
import com.techelevator.inventory.Listing;

public interface Reader {
	//done //declare method for stock which returns map<itemtype, item>
	Map <String, Listing> stock();

}

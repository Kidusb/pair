package com.techelevator.fileio;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.techelevator.inventory.Appetizer;
import com.techelevator.inventory.Beverage;
import com.techelevator.inventory.Dessert;
import com.techelevator.inventory.Entree;
import com.techelevator.inventory.Item;
import com.techelevator.inventory.Listing;

public class InventoryFileReader implements Reader {

	//all done!!
	//take in a file
	//break into list of lines 
	//create items from those lines
	//return a map<itemtype, item> inventory of items based on file
	
	
	private File fileName;

	public InventoryFileReader(File fileName) {
		this.fileName = fileName;
	}

	@Override
	public Map<String, Listing> stock() {
		List<String> lines = getLines(fileName);
		Map<String, Listing> inventory = getInventoryFromLines(lines);
		return inventory;
	}

	public List<String> getLines(File file) {
		List<String> lines = new ArrayList<String>();
		
		try (Scanner fileScanner = new Scanner (file)) {
			while(fileScanner.hasNextLine()) {
				String line = fileScanner.nextLine();
				lines.add(line);
			}
		} catch (FileNotFoundException e) {
			//todo address filenotfound exception
			e.printStackTrace();
		}
		
		return lines;
	}

	private Map<String, Listing> getInventoryFromLines(List<String> lines){
		Map<String, Listing> inventory = new LinkedHashMap<String, Listing>();

		for (String line : lines){
			String[] parts = line.split("\\|");
			Item item = createItemFromParts(parts);
			inventory.put(parts[0], new Listing(item));
		}

		return inventory;
	}

	private Item createItemFromParts(String[] parts) {
		
		String name = parts[1];
		BigDecimal price = new BigDecimal(parts[2]);
		String type = parts[3];
		
		Item item = null;

		if(type.equalsIgnoreCase("A")) {
			item = new Appetizer(name, price);
		}
		
		if(type.equalsIgnoreCase("B")) {
			item = new Beverage(name, price);
		}
		
		if(type.equalsIgnoreCase("D")) {
			item = new Dessert(name, price);
		}
		
		if(type.equalsIgnoreCase("E")) {
			item = new Entree(name, price);
		}

		return item;
	}
	

}

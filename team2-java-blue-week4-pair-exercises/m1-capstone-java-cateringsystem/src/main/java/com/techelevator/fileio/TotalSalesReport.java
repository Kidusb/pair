package com.techelevator.fileio;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.techelevator.inventory.Item;

public class TotalSalesReport {

	private Map<Item, Integer> itemsSold = new LinkedHashMap <Item, Integer>();

	public Map<Item, Integer> getItemsSold(){
		return itemsSold;
	}
	
	public BigDecimal getTotalSales() {
		BigDecimal totalSales = new BigDecimal("0.00");
		for (Entry<Item, Integer> entry : itemsSold.entrySet()) {
			BigDecimal itemPrice = entry.getKey().getPrice();
			BigDecimal groupPrice = itemPrice.multiply(new BigDecimal(entry.getValue()));
			totalSales = totalSales.add(groupPrice);
		}
		return totalSales;
	}
	
	public void addSale(Item item, int quantitySold ) {
		if (!itemsSold.containsKey(item)) {
			itemsSold.put(item, quantitySold);
		} else {
			int amtSold = itemsSold.get(item) + quantitySold;
			itemsSold.put(item, amtSold);
		}
	}

}

package com.techelevator.fileio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.techelevator.Action;
import com.techelevator.CateringSystem;

public class LogFileWriter implements Writer {

	private static final File LOG_FILE = new File("Log.txt");

	private List<Action> actions;

	public LogFileWriter(List<Action> actions) {
		this.actions = actions;
	}

	@Override
	public void write() throws IOException {		
		try (FileWriter fileWriter = new FileWriter(LOG_FILE, LOG_FILE.exists());
				BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
			for (Action action : actions) {
				bufferedWriter.write(action.getDate() + " " + action.getAction() + System.getProperty("line.separator"));
			}
		}
	}
}

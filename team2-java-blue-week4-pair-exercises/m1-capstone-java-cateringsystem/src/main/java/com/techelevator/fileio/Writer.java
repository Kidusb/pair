package com.techelevator.fileio;

import java.io.IOException;
import java.util.List;

public interface Writer {
	void write() throws IOException;
}

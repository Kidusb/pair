package com.techelevator;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.techelevator.cashflow.Register;
import com.techelevator.fileio.InventoryFileReader;
import com.techelevator.inventory.Item;
import com.techelevator.inventory.Listing;
import com.techelevator.view.Menu;
import com.techelevator.inventory.Cart;

public class CateringSystem {

	//todo update stock count
	//todo take order 
	//todo track actions as Action action = new Action (action taken) and add to actionlog


	private File file;
	private InventoryFileReader reader;
	private Menu menu;

	private Map<String, Listing> inventory;

	public CateringSystem(File file) {
		this.file = file;
		reader = new InventoryFileReader(file);
		menu = new Menu();
		setInventory();
	}

	private void setInventory() {
		inventory = reader.stock();
	}

	public Map<String, Listing> getInventory() {
		return inventory;
	}

	public String order(String productIdentifier, int quantity, Cart cart) {

		Listing listing = inventory.get(productIdentifier);

		if (listing.getStockCount() == 0) {
			return "Sold out. Unable to process order.";
		}  else if (listing.getStockCount() < quantity) {
			return "Insufficient Stock. Unable to process order.";
		} 

		else {
			cart.addItemToCart(listing.getItem(), quantity);
			listing.adjustStock(quantity);
			inventory.put(productIdentifier, listing);

			return "Added to cart. Cart total: $" + cart.getCartTotal();
		}
	}
}

package com.techelevator;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.techelevator.cashflow.Register;
import com.techelevator.fileio.LogFileWriter;
import com.techelevator.fileio.TotalSalesReport;
import com.techelevator.fileio.TotalSalesReportFileWriter;
import com.techelevator.inventory.Cart;
import com.techelevator.inventory.Listing;
import com.techelevator.view.Menu;

public class CateringSystemCLI {

	private static final String MAIN_MENU_DISPLAY_CATERING_ITEMS = "Display Catering Items";
	private static final String MAIN_MENU_ORDER = "Order";
	private static final String MAIN_MENU_QUIT = "Quit";
	private static final String[] MAIN_MENU_OPTIONS = { MAIN_MENU_DISPLAY_CATERING_ITEMS , MAIN_MENU_ORDER , MAIN_MENU_QUIT };

	private static final String ORDER_MENU_ADD_MONEY = "Add Money";
	private static final String ORDER_MENU_SELECT_PRODUCTS = "Select Products";
	private static final String ORDER_MENU_COMPLETE_TRANSACTION = "Complete Transaction";
	private static final String[] ORDER_MENU_OPTIONS = { ORDER_MENU_ADD_MONEY , ORDER_MENU_SELECT_PRODUCTS , ORDER_MENU_COMPLETE_TRANSACTION };


	private Menu menu;
	private CateringSystem cateringSystem;
	private Register register;
	private List<Action> actionLog;
	private LogFileWriter logWriter;
	private TotalSalesReport tsr;
	private TotalSalesReportFileWriter tsrWriter = new TotalSalesReportFileWriter();;

	public CateringSystemCLI(Menu menu) {
		this.menu = menu;
		register = new Register();
		actionLog = new ArrayList<Action>();
		tsr = new TotalSalesReport();
	}

	public void run() {

		//String path = menu.getInventoryFilePathFromUser();
		File inventoryFile = getFile();
		cateringSystem = new CateringSystem(inventoryFile);
		String userChoice = new String();

		while (!userChoice.equals(MAIN_MENU_QUIT)) {
			userChoice = mainMenu();
		}

		logWriter = new LogFileWriter(actionLog);
		tsrWriter.setTSR(tsr);
		
		try {
			logWriter.write();
			tsrWriter.write();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		Menu menu = new Menu();
		CateringSystemCLI cli = new CateringSystemCLI(menu);
		cli.run();
	}

	private String mainMenu() {

		String userChoice = menu.mainMenu(MAIN_MENU_OPTIONS);

		if (userChoice.equals(MAIN_MENU_DISPLAY_CATERING_ITEMS)) {
			menu.displayInventory(cateringSystem.getInventory(), true);
		} 

		if (userChoice.equals(MAIN_MENU_ORDER)) {
			orderMenu();
		}

		return userChoice;
	}

	private void orderMenu() {
		String userChoice = new String(); 
		Cart cart = new Cart();

		while(userChoice != null) {

			userChoice = menu.orderMenu(ORDER_MENU_OPTIONS, register.getBalance());
			
			if (userChoice.equalsIgnoreCase(ORDER_MENU_ADD_MONEY)) {
				addMoney();
			}

			if (userChoice.equalsIgnoreCase(ORDER_MENU_SELECT_PRODUCTS)) {
				menu.displayInventory(cateringSystem.getInventory(), false);
				selectProducts(cart);
			}

			if (userChoice.equalsIgnoreCase(ORDER_MENU_COMPLETE_TRANSACTION)) {
				completeTransaction(cart);

				userChoice = null;
			}
		}	
	}

	private File getFile() {
		File inventoryFile = null;

		while (inventoryFile == null) {
			String path = menu.getInventoryFilePathFromUser();
			inventoryFile = new File(path);

			if (!inventoryFile.exists() || !inventoryFile.isFile()) {
				menu.displayMessage(path + " is not a valid file.");
				inventoryFile = null;
			}
		}

		return inventoryFile;
	}
	
	private void addMoney() {
		BigDecimal depositAmount = menu.getDepositAmount();
		register.deposit(depositAmount);

		actionLog.add(new Action("ADD MONEY: $" + depositAmount + " $" + register.getBalance()));
	}
	
	private void selectProducts(Cart cart) {
		String orderItem = menu.getOrderItem();
		int itemQuantity = menu.getOrderQuantity();


		if (areValid(orderItem, itemQuantity)) {
			if (hasEnoughMoney(orderItem, itemQuantity)) {
				BigDecimal totalPrice = 
						cateringSystem.getInventory().get(orderItem).getItemPrice().multiply(new BigDecimal(itemQuantity));
				String orderResult = cateringSystem.order(orderItem, itemQuantity, cart);
				tsr.addSale(cateringSystem.getInventory().get(orderItem).getItem(), itemQuantity);
				menu.displayMessage(orderResult);
				register.charge(totalPrice);

				actionLog.add(new Action(
						itemQuantity + " " + cateringSystem.getInventory().get(orderItem).getItemName() 
						+ " " + orderItem + " " + totalPrice + " " + register.getBalance()));
			}

			if (!hasEnoughMoney(orderItem, itemQuantity)){
				menu.displayMessage("Insufficient balance. Please add money and try again.");
			}
		}
	}
	
	private void completeTransaction(Cart cart) {
		
		BigDecimal changeGiven = register.getBalance();

		Map<String, Integer> change = register.giveChange();
		menu.displayChange(change, changeGiven);
		menu.displayMessage("Current Account Balance: $" + register.getBalance() + "\n");
		menu.displayReport(cart);

		actionLog.add(new Action("GIVE CHANGE: $" + changeGiven + " $" + register.getBalance()));
		
	}
	
	private boolean areValid(String orderItem, int itemQuantity) {
		try {
			Listing listing = cateringSystem.getInventory().get(orderItem);
			
			if (cateringSystem.getInventory().get(orderItem).getStockCount() == 0) {
				menu.displayMessage("Sold out. Unable to process order.");
				return false;
			}

			if (cateringSystem.getInventory().get(orderItem).getStockCount() < itemQuantity) {
				menu.displayMessage("Insufficient Stock. Unable to process order.");
				return false;
			}
			
			return true;
		} catch (NullPointerException e) {
			menu.displayMessage("Invalid product selection. Unable to process order.");
			return false;
		}
		
	}
	
	private boolean hasEnoughMoney(String orderItem, int itemQuantity) {
		BigDecimal totalPrice = 
				cateringSystem.getInventory().get(orderItem).getItemPrice().multiply(new BigDecimal(itemQuantity));

		BigDecimal newBalance = register.getBalance().subtract(totalPrice);
		
		if (newBalance.compareTo(new BigDecimal("0.00")) != -1) {
			return true;
		} else {
			return false;
		}
		
	}
}

package com.techelevator.cashflow;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Register {
	

	private static final BigDecimal BILL_TWENTY = new BigDecimal("20.00");
	private static final BigDecimal BILL_TEN = new BigDecimal("10.00");
	private static final BigDecimal BILL_FIVE = new BigDecimal("5.00");
	private static final BigDecimal BILL_ONE = new BigDecimal("1.00");
	private static final BigDecimal COIN_QUARTER = new BigDecimal("0.25");
	private static final BigDecimal COIN_DIME = new BigDecimal("0.10");
	private static final BigDecimal COIN_NICKEL = new BigDecimal("0.05");
	private static final BigDecimal[] DENOMINATIONS = { BILL_TWENTY, BILL_TEN, BILL_FIVE, BILL_ONE, COIN_QUARTER, COIN_DIME, COIN_NICKEL};
	
	private static final String[] DENOMINATION_NAMES = {"$20 bill" , "$10 bill" , "$5 bill" , "$1 bill" , "quarter" , "dime" , "nickel"};
	
	private BigDecimal balance;
	
	
	public Register() {
		balance = new BigDecimal("0.00");
	}
	
	public BigDecimal getBalance() {
		return balance;
	}
	
	public void charge(BigDecimal amtPaid) {
		BigDecimal newBalance = balance.subtract(amtPaid);
		if (balanceIsInRange(balance) && !isNewBalanceNegative(newBalance)) {
			balance = balance.subtract(amtPaid);
		}
	}
	
	public void deposit(BigDecimal amtToDeposit) {
		BigDecimal newBalance = balance.add(amtToDeposit);
		if (balanceIsInRange(balance) && newBalanceIsInRange(newBalance)) {
			balance = balance.add(amtToDeposit);
		}
	}
	
	public Map<String, Integer> giveChange() {
		BigDecimal zero = new BigDecimal("0.00");

		Map<String, Integer> changeDenominations = new LinkedHashMap<String, Integer>();

		for (int i = 0 ; i < DENOMINATIONS.length ; i++) {
			BigDecimal remainingBalance = balance;
			int j = 0;
			while (remainingBalance.compareTo(DENOMINATIONS[i]) == 1 || remainingBalance.compareTo(DENOMINATIONS[i]) == 0) {
				BigDecimal newBalance = balance.subtract(DENOMINATIONS[i]);
				charge(DENOMINATIONS[i]);
				if (balance.compareTo(newBalance) == 0) {
					j++;
				}
				remainingBalance = balance;
			}
			
			if (j > 1) {
				changeDenominations.put(DENOMINATION_NAMES[i] + "s" , j);
			} else if (j > 0) {
				changeDenominations.put(DENOMINATION_NAMES[i] , j);
			} 	
		}

		return changeDenominations;

	}
	

	private boolean balanceIsInRange(BigDecimal balance) {
		if (balance.compareTo(new BigDecimal("5000.00")) == -1) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean newBalanceIsInRange(BigDecimal newBalance) {
		if ((newBalance.compareTo(new BigDecimal("5000.00")) == -1 
			    || newBalance.compareTo(new BigDecimal("5000.00")) == 0)
			&& (newBalance.compareTo(new BigDecimal("0.00")) != -1)) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean isNewBalanceNegative(BigDecimal newBalance) {
		if (newBalance.compareTo(new BigDecimal("0.00")) == -1) {
			return true;
		} else {
			return false;
		}
	}
	
}

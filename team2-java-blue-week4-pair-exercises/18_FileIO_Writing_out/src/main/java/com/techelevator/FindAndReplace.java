package com.techelevator;

import java.io.File;

public class FindAndReplace {

	private Menu menu = new Menu();
	
	public void run() {
		
		//Prompt user for file
		File sourceFile = getSourceFile();
		//Prompt user for search word
		String searchTerm = menu.getSearchTerm();
		//Prompt user for word to replace search word
		String replacementTerm = menu.getReplacementTerm();
		//Prompt user for destination file
		File destinationFile = getDestinationFile(sourceFile);
		//Replace search term with replacement term
		Replace replace = new Replace(sourceFile, destinationFile, searchTerm, replacementTerm); 
		replace.write();
		//Inform user task is completed
		menu.display("Replacement Complete!");
		
	}
	
	private File getSourceFile() {
		File file = null;
		String filename = menu.getSourceFileFromUser();
		file = new File (filename);
		if(!file.exists() || !file.isFile() ) {
			menu.display("Invalid Source File");
			System.exit(1);
		} 
			return file;
	}
	private File getDestinationFile(File sourceFile) {
		File file = null;
		String filename = menu.getDestinationFileFromUser();
		file = new File (filename);
		if( file.equals(sourceFile) ) {
			menu.display("Invalid Destination File");
			System.exit(1);
		} 
			return file;
	}
	
	public static void main(String[] args) {

		FindAndReplace far = new FindAndReplace();
		
		far.run();
		
	}

}

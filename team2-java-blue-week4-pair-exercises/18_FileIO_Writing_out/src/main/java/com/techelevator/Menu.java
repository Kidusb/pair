package com.techelevator;

import java.util.Scanner;

public class Menu {
	
	private Scanner in = new Scanner(System.in);
	
	public String getSourceFileFromUser() {
		System.out.println("What is the source file? ");
		return in.nextLine();
	}
	public String getDestinationFileFromUser() {
		System.out.println("What is the destination file? ");
		return in.nextLine();
	}
	public String getSearchTerm() {
		System.out.println("What term would you like to search for: ");
		return in.nextLine();
	}
	public String getReplacementTerm() {
		System.out.println("What term would you like to replace it with: ");
		return in.nextLine();
	}
	public void display(String message) {
		System.out.println(message);
		System.out.flush();
	}

}

package com.techelevator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Replace {

	private String searchTerm;
	private String replacementTerm;
	private File sourceFile;
	private File destinationFile;
	private Menu menu = new Menu();
	 
	public Replace (File sourceFile, File destinationFile, String searchTerm, String replacementTerm) {
		this.sourceFile = sourceFile;
		this.destinationFile = destinationFile;
		this.searchTerm = searchTerm;
		this.replacementTerm = replacementTerm;
		
	}
	public void write() {
		List<String> lines = getLines(searchTerm, replacementTerm);
		try(PrintWriter printwriter = new PrintWriter(destinationFile);
				BufferedWriter writer = new BufferedWriter(printwriter) ) {
			for (String line : lines) {
				writer.write(line + System.getProperty("line.separator") );
			} 

		} catch (IOException e) {
			menu.display("IOException occured: " + e.getMessage() );
		}
	}

	
	private List<String> getLines (String searchTerm, String replacementTerm) {
		List<String> lines = new ArrayList<String>();
		try(Scanner fileScanner = new Scanner(sourceFile) ) {
			while (fileScanner.hasNextLine() ) {
				String line = fileScanner.nextLine();
				line = line.replace(searchTerm, replacementTerm);
				lines.add(line);
			}
		}catch (FileNotFoundException e) {
			e.getStackTrace();
		}
		
		
		return lines;
		
		
	}
	
}
